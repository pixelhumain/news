<?php
// gettting asstes from parent module repo
$cssAnsScriptFilesModule = array( 
  '/css/news.css',
  '/css/timeline.css',
  '/css/form.css',
  '/js/news.js',
  '/js/init.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( News::MODULE )->getAssetsUrl() );


$cssAnsScriptFilesModule = array(
  "css/default/directory.css",
  "js/comments.js",
  "css/menus/multi_tags_scopes.css",
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule,  Yii::app()->theme->baseUrl."/assets/" );
$cssAnsScriptFilesModule = array(
  '/js/scopes/scopes.js',
  '/js/default/directory.js',
  '/js/links.js',
  '/js/default/search.js',
  '/js/default/globalsearch.js',
  
  '/js/menus/multi_tags_scopes.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl() );
$cssAnsScriptFilesModule = array(
  '/js/uiModeration.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "dda" )->getAssetsUrl() );
$cssAnsScriptFiles = array(
    '/plugins/moment/min/moment.min.js' ,
    '/plugins/moment/min/moment-with-locales.min.js',
    '/plugins/jquery.dynForm.js',
    '/plugins/select2/select2.min.js' , 
    '/plugins/select2/select2.css',
    '/plugins/jquery.elastic/elastic.js',
    '/plugins/underscore-master/underscore.js',
    '/plugins/jquery-mentions-input-master/jquery.mentionsInput.js',
    '/plugins/jquery-mentions-input-master/jquery.mentionsInput.css',
    '/plugins/fine-uploader/jquery.fine-uploader/fine-uploader-gallery.css',
    '/plugins/fine-uploader/jquery.fine-uploader/jquery.fine-uploader.js',
    '/plugins/fine-uploader/jquery.fine-uploader/fine-uploader-new.min.css',
    '/plugins/facemotion/faceMocion.css',
    '/plugins/facemotion/faceMocion.js',
    '/plugins/showdown/showdown.min.js',
    '/plugins/to-markdown/to-markdown.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->request->baseUrl);

?>
<style>
    .timeline > li{
      width:100%;
    }
    .timeline::before {
      left:0;
    }
    #noMoreNews{
    	display: none;
    }
</style>

<div class="row bg-white">

  <div class="col-xs-12 margin-top-70">
    <!-- <h3 class="text-center"><i class="fa fa-newspaper-o"></i> Message</h3><hr> -->
  </div>
<div class="container">
	<ul class="timeline inline-block" id="news-list">
	<?php if(!empty($element)){
      echo $this->renderPartial('news.views.co.timelineTree', 
								array("news"=>array( @(string)$element["_id"] => $element), 
									  "nbCol"=> 1,
                    "endStream"=>true,
                    "inline"=>true) );  
      if( @Yii::app()->session["userId"] ) echo $this->renderPartial('news.views.co.modalShare', array());

    }else{ ?>
    <p class="col-md-12 col-sm-12 col-xs-12">
      <?php echo Yii::t("news","This link points to a dead news")?>.<br>
      <?php echo Yii::t("news","The author has probably deleted the news") ?> !!<br>
      <?php if (@Yii::app()->session["userId"]){ ?>
        <a href="#page.type.<?php echo Person::COLLECTION ?>.id.<?php echo Yii::app()->session["userId"] ?>" class="btn btn-warning lbh"><i class="fa fa-sign-in"></i> <?php echo Yii::t("news","Back to my page") ?></a>
      <?php } else { ?> 
        <a href="#" class="btn btn-warning lbh"><i class="fa fa-sign-in"></i> <?php echo Yii::t("news","Back to home") ?></a>
      <?php } ?>
    </p>
    <?php }
	?>
	</ul>
</div>
</div>
<script type="text/javascript">
  var nbCol=1;
  var inline=true;
	var news=<?php echo json_encode($element); ?>;
	jQuery(document).ready(function() {	
   /*  if(news){
      if(typeof news.text != "undefined"){
        var text = news.text.substr(0,30);
        if(news.text.length>30) text+="...";
        setTitle("", "", text);

        if(typeof news.mentions != "undefined")
          text = mentionsInit.addMentionInText(news.text,news.mentions);
        else text = news.text;
      }

  	  <?php if(isset(Yii::app()->session["userId"])) { ?>
        initCommentsTools(new Array(news));
      <?php } ?>
      
      $(".timeline_text").html(linkify(text));
    	//showCommentsTools(news["_id"]['$id']);
    } */
	});

</script>