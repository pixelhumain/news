<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;
use CAction;
use News;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\tasks\TaskQueue;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Preference;
use Rest;
use Yii;

class SaveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id=null)
    {
    	$controller=$this->getController();
        $result=array("result"=>false, "msg"=>Yii::t("common","You must be logged in to add a news entry !"));
    	if(Person::logguedAndValid()){
            $result=News::save($_POST);

            $user = Person::getById(Yii::app()->session["userId"]);
            $taskQueue = new TaskQueue();
            if (Preference::isActivitypubActivate($user["preferences"])) {
                $taskData = [
                    'type' => 'createNoteActivity',
                    'user' => $user['_id'],
                    'data' => $_POST,
                    'newsId' => (string)$result["object"]["_id"]
                ];
                $taskQueue->addTask($taskData);
            }

        	if(!@$_REQUEST["json"]){
        		$params=array(
        			"news"=>array( (string)$result["id"]=>$result["object"]),
        			"actionController"=>"save",
        			"canManageNews"=>true,
        			"canPostNews"=>true,
                    "endStream"=>false,
                    "pair" => false);
    			return $controller->renderPartial("news.views.co.timelineTree", $params,true);
        	}else
                return Rest::json($result);
        }else
            return Rest::json($result);
    }
}
