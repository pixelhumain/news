<style>
    .newsContent p{
        font-size: 18px !important;
    }

    .timeline-footer .tpap-actions{
        margin: 0;
        padding: 10px 0px;
        font-weight: 700;
        font-size: 18px;
    }

    .timeline-footer .tpap-actions li{
        display: inline-block;
        margin-right: 10px;
    }

    .timeline-footer .tpap-actions li a{
        text-decoration: none;
    }

    .timeline-footer .tpap-actions li a span{
        margin-left: 5px;
    }

    .provider-info{
        display: flex;
        align-items: center;
    }

    .provider-info div{
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }

    .provider-info div span{
        font-size: 11px;
        background-color: #777;
        color: white;
        padding: 1px 4px;
        border-radius: 2px;
    }

    .provider-info img{
        height: 30px;
        margin-left: 5px;
    }

    .provider-info a{
        text-decoration: none;
        font-size: 14px;
        color: #777;
        transition: .2s;
    }

    .provider-info a:hover{
        color: #222;
    }
</style>
<div class="timeline-heading text-center ">
    <h5 class="text-left srcMedia">
        <?php $author = $media["note"]["author"]; ?>
        <small>
            <img class="pull-left img-circle" src="<?= $author['avatar'] ?>" height="40">

            <div class="pull-left padding-5 col-md-7 col-sm-7 text-left" style="line-height: 15px;">
                <a href="<?= $author['id'] ?>" target="__blank"><?= $author['name'] ?></a>
                <br>
                <span class="margin-top-5">
                    <a href="<?= $author['id'] ?>" target="__blank"><?= $author['address'] ?></a>
                </span>
            </div>
        </small>
        <span class="margin-top-10 margin-bottom-10 pull-right">
            <small id="time-<?= $key ?>" data-updated="<?= $media["created"]->sec ?>"></small>
        </span>
    </h5>
</div>
<div class="timeline-body  col-xs-12 text-left margin-bottom-15 margin-top-15">
    <div class="newsContent no-padding"><?= $media["note"]["content"] ?></div>
    <?php foreach($media["note"]["attachments"] as $attachment){ ?>
        <div class="bg-white results col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="col-md-12 no-padding margin-top-10 margin-bottom-10">
                <a class="thumb-info" href="<?= $attachment ?>" target="__blank">
                    <img src="<?= $attachment ?>" class="img-responsive"/>
                </a>
            </div>
        </div>
    <?php } ?>
</div>
<div class="timeline-footer pull-left col-md-12 col-sm-12 col-xs-12 padding-top-5">
    <div class="col-xs-12 pull-left padding-5" style="display: flex; justify-content:space-between; align-items:center;">
        <ul class="tpap-actions">
            <li>
                <a href="javascript:;" class="newsAddComment" data-media-id="<?= $key ?>" data-type="activitypub">
                    <i class="fa fa-comment"></i>
                    <?=($media["note"]["nbReplies"]>0)?'<span>'.$media["note"]["nbReplies"].'</span>':'' ?>
                    Commentaire
                </a>
            </li>
            <!-- <li><a href="javascript:;" class="letter-blue"><i class="fa fa-thumbs-up"></i><span>5</span> J’aime</span></a></li> -->
        </ul>
        <div class="provider-info">
            <?php $providerInfo = $media["note"]["providerInfo"] ?>
            <div>
                <span>envoyé depuis</span>
                <a href="<?= $media["note"]["url"] ?>" target="__blank">
                    <?= $providerInfo["title"] ?>
                </a>
            </div>
            <?php if(isset($providerInfo["icon"])){ ?>
            <img src="<?= $providerInfo["icon"] ?>" alt="">
            <?php } ?>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding pull-left margin-top-10" id="commentContent<?= $key ?>"></div>
</div>
<script>
    $(function(){
        $el = $("#time-<?= $key ?>")
        $time = directory.showDatetimePost(null, "<?=$key?>", $el.data("updated"))
        $el.html($time)
    })
</script>
