<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, DataValidator, Yii, Person, News, ActStr, Rest;
class ShareAction extends \PixelHumain\PixelHumain\components\Action
{
	 /**
	 * TODO Clement : La PHPDOC
	 */
    public function run() {
    	if(DataValidator::missingParamsController($_POST, ["childType","parentId","parentType"]) )
    		return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));

	    $result = array("result"=>false, "msg"=>Yii::t("common", "Incorrect request"));
		
		if ( ! Person::logguedAndValid() ) {
			return array("result"=>false, "msg"=>Yii::t("common", "You are not loggued or do not have acces to this feature "));
		}


	    $parentId = $_POST["parentId"];
    	$parentType = $_POST["parentType"];
    	
	    $child = array(
			"id" => @$parentId,
	    	"type" => $parentType
	    );
    	
    	//le cas du partage de news
    	//on rajoute l'identité de l'auteur de la news
	    if($parentType == "news"){
			$object=News::getById($parentId);
			$authorNews=Person::getById(@$object["author"]["id"]);
			$child["authorName"] = @$authorNews["name"];
			$child["authorId"] = @$object["author"]["id"];
		}
		
    	$result = News::share(ActStr::VERB_SHARE,@$_POST["childId"],@$_POST["childType"], @$_POST["comment"], $child);
		return Rest::json($result);
    }

}