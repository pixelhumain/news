<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, Element, Rest, Yii, MongoId, Citoyen, Log, Person, Authorisation;
use PHDB;

class GetInfoBeforeAddSpamElementAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {

        if ( ! Person::logguedAndValid() ) 
            return Rest::json( array( "result" => false, "msg" => "You must be logged to do this action !" ));
        if ( !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) 
			return Rest::json( array( "result" => false, "msg" => "You must be logged as an Superadmin user to do this action !" ));


        $id = $_POST["id"];
        $type = $_POST["type"];

        $userId = $id;

        
        $res=array();
        $res["type"]= $type;
        $res["id_element"]= $id;

        if($type != Citoyen::COLLECTION){
            $creator = Element::getCreatorElement($type, $id);
            

            foreach($creator as $idCreator => $creatorElement){
                $res["creator"]= $creatorElement;
                $userId = $idCreator; 

                if(isset($creatorElement["links"])){
                    $allLinks = Element::getAllLinks($creatorElement["links"],Citoyen::COLLECTION, $idCreator);
                    $res["linksOfCreator"]= array();

                    foreach ($creatorElement["links"] as $linkType => $aLink) {
                        foreach ($aLink as $linkElementId => $linkInfo) {
                            $collection = $linkInfo["type"];
                            
                            if(!isset($res["linksOfCreator"][$collection]))
                                $res["linksOfCreator"][$collection] = array();


                            if(isset($allLinks[$linkElementId]))
                                $res["linksOfCreator"][$collection][$linkElementId] = $allLinks[$linkElementId];
                        }
                    }
                }
            }
        }

        $element = Element::getByTypeAndId( $type, $id);
        
        $res["element"] = $element;

        if((isset($element["slug"]) && $element["slug"] != "unknown") || (isset($element["collection"]) && $element["collection"] == "poi")){
            if(!empty($element) &&  isset($element["name"]) ){
                $res["nameElement"]= $element["name"];
            }

            
            $res["logs"] = array();
    
            $aggregate= array(
                array( '$group' => array(
                        '_id' => array(
                            'action' => '$action'
                            , 'ipAddress' => '$ipAddress'
                            , 'result' => '$result.result'
                            , 'msg' => '$result.msg'
                            , 'userId' => '$userId'
                        ),
                        'count' => array ('$sum' => 1),
                        'minDate' => array ( '$min' => '$created' ),
                        'maxDate' => array ( '$max' => '$created' )
                    )
                ),
                array( '$match' => array("_id.userId" => $userId) ),
                array( '$project' => array("action" => '$_id.action', "count" => '$count', "ipAddress" => '$_id.ipAddress', "result" => '$_id.result', "msg" => '$_id.msg', "userId" => '$_id.userId',) ),
            );
    
            $resultLog = PHDB::aggregate(Log::COLLECTION, $aggregate);

            if(isset($resultLog["result"]) && !empty($resultLog["result"]))
                $res["logs"] = $resultLog["result"];


            $res["ipspam"] = array();
            foreach($res["logs"] as $index => $log){
                if(isset($log["ipAddress"]) && $log["ipAddress"] != null && $log["ipAddress"] != "" && $log["ipAddress"] != "127.0.0.1" && $log["ipAddress"] != "localhost" ){
                    $res["ipspam"][$log["ipAddress"]] = $log["ipAddress"];
                }
            }
            
            if(isset($element["links"])){
                $allLinks = Element::getAllLinks($element["links"],$type, $id);
                $res["allLinks"] = array();

                foreach ($element["links"] as $linkType => $aLink) {
                    foreach ($aLink as $linkElementId => $linkInfo) {
                        $collection = $linkInfo["type"];
                        
                        if(!isset($res["allLinks"][$collection]))
                            $res["allLinks"][$collection] = array();

                        if(isset($allLinks[$linkElementId]))
                            $res["allLinks"][$collection][$linkElementId] = $allLinks[$linkElementId];
                    }
                }
            }
		}
        $res["impacte"] = $this->getImpacteSpam($type, $id);
        $res["analyse"] = Element::analyseSpamElement( $type, $id);
		
		return Rest::json($res);
    }


    private function getImpacteSpam($typeElement, $idElement, $ipAdresse = array()) {
        $impacte = 5;
        $analyse = Element::analyseSpamElement( $typeElement, $idElement);
        $creator = Element::getCreatorElement($typeElement, $idElement);
            
        foreach($analyse as $key => $val){
            if(isset($analyse[$key]) && $analyse[$key] != null && isset($analyse[$key]["count"]))
                $impacte = (int) round(($impacte - 1) - ($analyse[$key]["count"] / 10), 0); 
        }

        foreach($creator as $idCreator => $creatorElement){
            $analyseCreator = Element::analyseSpamElement( Citoyen::COLLECTION, $idCreator);
            foreach( $analyseCreator as $key => $val){
                if(isset($analyseCreator[$key]) && $analyseCreator[$key] != null && isset($analyseCreator[$key]["count"]))
                    $impacte = (int) round(($impacte - 1) - ($analyseCreator[$key]["count"] / 10), 0); 
            }
        }

        foreach($ipAdresse as $ip){
            $ipCount = PHDB::find("ipSpam", [
                "ip" => $ip
            ]);
            foreach($ipCount as $c){
                $impacte += 2;
            }
        }
        

        
            
        if($impacte > 5)
            $impacte = 5;
        else if($impacte < 0)
            $impacte = 0;

        return $impacte;
    }
}