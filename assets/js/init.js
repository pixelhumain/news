/* ******************
CO.js
********************* */

function escapeHtml(string) {
	var entityMap = {
	    '"': '&quot;',
    	"'": '&#39;',
	};
    return String(string).replace(/["']/g, function (s) {
        return entityMap[s];
    });
} 
/* *********************************
			COLLECTIONS
********************************** */

var collection = {
	crud : function (action, name,type,id) { 
		if(userId){
			var params = {};
			var sure = true;
			if(typeof type != "undefined")
				params.type = type;
			if(typeof id != "undefined")
				params.id = id;
			if(typeof action == "undefined")
				action = "new";
			if(action == "del"){
				params.name = name;
				sure = confirm("Vous êtes sûr ?");
			}
			else if(action == "new" || action == "update")
				params.name = prompt(tradDynForm.collectionname+' ?',name);
			if(action == "update")
				params.oldname = name;
			
			if(sure)
			{
				ajaxPost(null,baseUrl+"/"+moduleId+"/collections/crud/action/"+action ,params,function(data) { 
					console.warn(params.action);
					if(data.result){
						toastr.success(data.msg);
						if(location.hash.indexOf("#page") >=0){
							loadDataDirectory("collections", "star");
						}
						//if no type defined we are on user
						//TODO : else add on the contextMap
						if( typeof type == "undefined" && action == "new"){
							if(!userConnected.collections)
								userConnected.collections = {};
							if(!userConnected.collections[params.name])
								userConnected.collections[params.name] = {};
						}else if(action == "update"){
							smallMenu.openAjax(baseUrl+'/'+moduleId+'/collections/list/col/'+params.name,params.name,'fa-folder-open','yellow');
							if(!userConnected.collections[params.name])
								userConnected.collections[params.name] = userConnected.collections[ params.oldname ];
							delete userConnected.collections[ params.oldname ];
						}else if(action == "del"){
							delete userConnected.collections[params.name];
							smallMenu.open();
						}
						collection.buildCollectionList("col_Link_Label_Count",".menuSmallBtns", function() { $(".collection").remove() })
					}
					else
						toastr.error(data.msg);
				}, "none");
			}
		} else
			toastr.error(trad.LoginFirst);
	},
	applyColor : function (what,id,col) {
		var collection = (typeof col == "undefined") ? "favorites" : col;
		if(userConnected && userConnected.collections && userConnected.collections[collection] && userConnected.collections[collection][what] && userConnected.collections[collection][what][id] ){
			$(".star_"+what+"_"+id).children("i").removeClass("fa-star-o").addClass('fa-star text-red');
		}
	},
	isFavorites : function (type, id){
		res=false;
		if(userConnected && userConnected.collections){
			$.each(userConnected.collections, function(name, listCol){
				if(typeof listCol[type] != "undefined" && typeof listCol[type][id] != "undefined"){
					res=name;
					return false;
				}
			});
		}
		return res;
	},
	add2fav : function (what,id,col){
		var collection = (typeof col == "undefined") ? "favorites" : col;
		if(userId){
			var params = { id : id, type : what, collection : collection };
			var el = ".star_"+what+"_"+id;
			
			ajaxPost(null,baseUrl+"/"+moduleId+"/collections/add",params,function(data) { 
				console.warn(params.action,collection,what,id);
				if(data.result){
					if(data.list == '$unset'){
						/*if(location.hash.indexOf("#page") >=0){
							if(location.hash.indexOf("view.directory.dir.collections") >=0 && contextData.id==userId){ 
                				loadDataDirectory("collections", "star"); 
              				}else{ 
                				$(".favorisMenu").removeClass("text-yellow"); 
                				$(".favorisMenu").children("i").removeClass("fa-star").addClass('fa-star-o'); 
              				} 
						}else{*/
							$(el).removeClass("letter-yellow-k"); 
							$(el).find("i").removeClass("fa-star letter-yellow-k").addClass('fa-star-o');
							delete userConnected.collections[collection][what][id];
						//}
					}
					else{
						/*if(location.hash.indexOf("#page") >=0){
							if(location.hash.indexOf("view.directory.dir.collections") >=0 && contextData.id==userId){ 
                				loadDataDirectory("collections", "star"); 
              				}else{ 
                				$(".favorisMenu").addClass("text-yellow"); 
                				$(".favorisMenu").children("i").removeClass("fa-star-o").addClass('fa-star'); 
              				}
              			}
						else*/
							$(el).addClass("letter-yellow-k"); 
							$(el).find("i").removeClass("fa-star-o").addClass('fa-star letter-yellow-k');

						if(!userConnected.collections)
							userConnected.collections = {};
						if(!userConnected.collections[collection])
							userConnected.collections[collection] = {};
						if(!userConnected.collections[collection][what])
							userConnected.collections[collection][what] = {};
						userConnected.collections[collection][what][id] = new Date();	
					}
					toastr.success(data.msg);
				}
				else
					toastr.error(data.msg);
			},"none");
		} else
			toastr.error(trad.LoginFirst);
	},
	buildCollectionList : function ( tpl, appendTo, reset ) {
		if(typeof reset == "function")
			reset();
		str = "";
		$.each(userConnected.collections, function(col,list){ 
			var colcount = 0;
			$.each(list, function(type,entries){
				colcount += Object.keys(entries).length;
			}); 
			str += js_templates[ tpl ]({
				label : col,
				labelCount : colcount
			}) ;
		});
		$(appendTo).append(str);
	}
};

var mentionsInput=[];
var mentionsInit = {
	stopMention : false,
	isSearching : false,
	keyUpTimeOut:null,
	get : function(domElement){
		mentionsInput=[];
		$(domElement).mentionsInput({
			allowRepeat:true,
		 	onDataRequest:function (mode, query, callback) {
				clearInterval(mentionsInit.keyUpTimeOut);
				mentionsInit.keyUpTimeOut = setTimeout(function(){
					mentionsInit.isSearching=true;
			   		var search = {"searchType" : ["citoyens","organizations","projects"], "name": query};
			   		ajaxPost(
				        null,
				        baseUrl+"/" + moduleId + "/search/globalautocomplete",
				        search,
				        function(retdata){ 
				        	if(!retdata){
				        		toastr.error(retdata.content);
				        	}else{
					        	var data = [];
						        $.each(retdata.results, function (e, value){
									avatar="";
									if(typeof value.profilThumbImageUrl != "undefined" && value.profilThumbImageUrl!="")
										avatar = baseUrl+value.profilThumbImageUrl;
									object = new Object;
									object.id = e;
									object.name = value.name;
									object.slug = value.slug;
									object.avatar = avatar;
									object.type = value.collection;
									data.push(object);
					        	});
					    		data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
								callback.call(this, data);
				  			}
				        }
			        );
				}, 300)
			}
	  	});
	},
	beforeSave : function(object, domElement){
		$(domElement).mentionsInput('getMentions', function(data) {
			mentionsInput=data;
		});
		if (typeof mentionsInput != "undefined" && mentionsInput.length != 0){
			var textMention="";
			$(domElement).mentionsInput('val', function(text) {
				textMention=text;
				
				$.each(mentionsInput, function(e,v){
					strRep=v.name;
					while (textMention.indexOf("@["+v.name+"]("+v.type+":"+v.id+")") > -1){
						if(typeof v.slug != "undefined")
							strRep="@"+v.slug;
						textMention = textMention.replace("@["+v.name+"]("+v.type+":"+v.id+")", strRep);
					}
				});
			});			
			object.mentions=mentionsInput;
			object.text=textMention;
		}
		return object;		      		
	},
	addMentionInText: function(text,mentions){
		$.each(mentions, function( index, value ){
			if(typeof value.slug != "undefined"){
				while (text.indexOf("@"+value.slug) > -1){
					str="<span class='lbh' onclick='urlCtrl.loadByHash(\"#page.type."+value.type+".id."+value.id+"\")' onmouseover='$(this).addClass(\"text-blue\");this.style.cursor=\"pointer\";' onmouseout='$(this).removeClass(\"text-blue\");' style='color: #719FAB;'>"+
			   						value.name+
			   					"</span>";
					text = text.replace("@"+value.slug, str);
				}
			}else{
				//Working on old news
		   		array = text.split(value.value);
		   		text=array[0]+
		   					"<span class='lbh' onclick='urlCtrl.loadByHash(\"#page.type."+value.type+".id."+value.id+"\")' onmouseover='$(this).addClass(\"text-blue\");this.style.cursor=\"pointer\";' onmouseout='$(this).removeClass(\"text-blue\");' style='color: #719FAB;'>"+
		   						value.name+
		   					"</span>"+
		   				array[1];
		   	}   					
		});
		return text;
	},
	reset: function(domElement){
		$(domElement).mentionsInput('reset');
	}
}

