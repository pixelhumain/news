<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, Element, Rest, Yii, MongoId, Citoyen, Log, Person, Authorisation;
use PHDB;

class GetInfoBeforeDeleteSpamElementAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {

        if ( ! Person::logguedAndValid() ) 
            return Rest::json( array( "result" => false, "msg" => "You must be logged to do this action !" ));
        if ( !Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) 
			return Rest::json( array( "result" => false, "msg" => "You must be logged as an Superadmin user to do this action !" ));


        $id = $_POST["id"];
        $type = $_POST["type"];
        
        $res= Element::getInfoBeforeDelete($type, $id );

        
		return Rest::json($res);
    }
}