<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;
use Authorisation;
use CAction;
use Document;
use Element;
use Event;
use Link;
use Organization;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use Project;
use Tags;
use Yii;
use CacheHelper;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=null, $id= null, $scroll=true, $date = null, $isLive = null, $source=null,$streamType="news", $textSearch=null, $nbCol=null, $inline=null, $blog=false){
    	$ctrl = $this->getController();
     	$ctrl->layout = "//layouts/empty";
     	$news = array();
		$params=array(
			"contextParentType" => $type, 
			"contextParentId" => $id,
			"source"=>$source,
			"authorizedToStock"=> Document::authorizedToStock($id, $type,Document::DOC_TYPE_IMAGE),			
			"tags" => Tags::getActiveTags(),
			"edit" => false,
			"openEdition" => false,
			"canPostNews"=>false,
			"canAddNews"=>false,
			"canManageNews"=>false,
			"formCreate"=>true,
			"indexStep"=>6,
			"nbCol"=>2,
			"inline"=> false,
			"scroll"=>$scroll,
			"search"=>false,
			"clickEvent"=>false
		);
		if(@$parent){
			$params["edit"] = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $parent["_id"]);
			$params["openEdition"] = Authorisation::isOpenEdition($parent["_id"], $type, @$parent["preferences"]);
		}
		if(@Yii::app()->session["userId"]){
			$params["canPostNews"]=true;
			if(in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])){
				$parent = Element::getElementSimpleById($id,$type,null, array("links", "name", "profilThumbImageUrl"));
				if($type!= Event::COLLECTION && @$parent["links"][Element::$connectTypes[$type]][Yii::app()->session["userId"]] 
					&& !@$parent["links"][Element::$connectTypes[$type]][Yii::app()->session["userId"]][Link::TO_BE_VALIDATED])
					$params["canManageNews"]=true;
				else if($type==Event::COLLECTION && ((@$parent["links"]["attendees"][Yii::app()->session["userId"]] && @$parent["links"]["attendees"][Yii::app()->session["userId"]]["isAdmin"]) || @$parent["links"]["organizer"][Yii::app()->session["userId"]]) )
            		$params["canManageNews"]=true;
            }else if($type == Person::COLLECTION && Yii::app()->session["userId"]==$id && $isLive!=true){
					$params["canManageNews"]=true;
			}
			if($type == Person::COLLECTION){
				$params["canAddNews"] = true;
			}else if(in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])){
				$params["canAddNews"] = Authorisation::canParticipate(Yii::app()->session["userId"], $type, $id, false);
				$costum = CacheHelper::getCostum();
				if($costum){
					if(isset($costum["news"]["canPostNews"]["roles"])){
						$params["canAddNews"] = Authorisation::hasRoles($type,$id,$costum["news"]["canPostNews"]);
						//$params["canAddNews"] = Authorisation::hasRolesCostum($costum["news"]["canPostNews"]);
					}
					
				}
				
				
			}
        }
        $params["parent"]=@$parent;
		$params["isLive"]=(@$isLive && $isLive != null) ? $isLive : false;
		if(@$_POST){
			$params["formCreate"] = @$_POST["formCreate"] ? $_POST["formCreate"] : true;
			$params["timelineHtml"] = @$_POST["timelineHtml"] ? $_POST["timelineHtml"] : null;
			$params["nbCol"] = @$_POST["nbCol"] ? $_POST["nbCol"] : 2;
			$params["inline"] = (isset($_POST["inline"]) && ($_POST["inline"]===true || $_POST["inline"]=="true" )) ? true : false;
			$params["indexStep"] = @$_POST["indexStep"] ? $_POST["indexStep"] : 6;
			$params["typeNews"] = @$_POST["typeNews"] ? $_POST["typeNews"] : null;
			$params["scroll"] = @$_POST["scroll"] ? $_POST["scroll"] : $scroll;
			$params["scrollDom"] = @$_POST["scrollDom"] ? $_POST["scrollDom"] : null;
			$params["members"] = @$_POST["members"] ? $_POST["members"] : false;
			$params["search"] = @$_POST["search"] ? $_POST["search"] : false;
			$params["clickEvent"] = @$_POST["clickEvent"] ? $_POST["clickEvent"] : false;
			$params["searchTags"] = @$_POST["searchTags"] ? $_POST["searchTags"] : null;
			$params["searchTypes"] = @$_POST["searchTypes"] ? $_POST["searchTypes"] : null;
			
            // activitypub
            $params["fediTags"] = @$_POST["fediTags"] ? $_POST["fediTags"] : null;
			//$params["scope"] = @$_POST["scope"] ? $_POST["scope"] : null;
		} else if (@$_GET){
			$params["formCreate"] = @$_GET["formCreate"] ? $_GET["formCreate"] : true;
			$params["nbCol"] = @$_GET["nbCol"] ? $_GET["nbCol"] : 2;
			$params["inline"] = @$_GET["inline"] ? $_GET["inline"] : false;
			$params["members"] = @$_POST["members"] ? $_POST["members"] : false;
			$params["indexStep"] = @$_GET["indexStep"] ? $_GET["indexStep"] : 6;
			$params["typeNews"] = @$_GET["typeNews"] ? $_GET["typeNews"] : null;
			$params["searchTags"] = @$_GET["searchTags"] ? $_GET["searchTags"] : null;
			$params["scroll"] = @$_GET["scroll"] ? $_GET["scroll"] : $scroll;
			$params["scrollDom"] = @$_GET["scrollDom"] ? $_GET["scrollDom"] : null;
			$params["clickEvent"] = @$_GET["clickEvent"] ? $_GET["clickEvent"] : false;
		}

        $params['tagFediverses'] = $type== 'citoyens' ? ActivitypubLink::getAllTags() : [];

		if(!empty($nbCol))
			$params["nbCol"]=$nbCol;
		if(!empty($inline))
			$params["inline"]=$inline;
		if (in_array($type,[Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION, Person::COLLECTION]))
			$params["deletePending"] = Element::isElementStatusDeletePending($type, $id);

		$tpl=($blog==true) ? "news.views.co.blog" : "news.views.co.index";
		

		if(Yii::app()->request->isAjaxRequest)
            return $ctrl->renderPartial($tpl, $params);
        else 
            return $ctrl->render($tpl, $params);
    }
}