<style>

.timeline-panel .searchEntityContainer{
  width: 100% !important;
  margin-top:20px;
  max-height: 450px !important;
  margin-bottom: 15px !important;
  min-height: unset !important;
}
.timeline-body .btn-share{
  display: none;
}
.timeline-panel .container-img-profil{
  max-height: 250px;
}

.timeline-panel#nbAbuse3,
.timeline-panel#nbAbuse4 {
  background-color: #ffe4e4 !important;
  border: 2px solid #ffacac;
}

.timeline-panel#nbAbuse3 .timeline_text{
  color: #c29898 !important;
}

.timeline-panel#nbAbuse4 .timeline_text{
  color: #dacbcb !important;
}
.timeline-panel#nbAbuse3::before,
.timeline-panel#nbAbuse4::before{
  border-left: 15px solid #ffacac;
  border-left-width: 15px;
}

li.timeline-inverted .timeline-panel#nbAbuse3::before,
li.timeline-inverted .timeline-panel#nbAbuse4::before{
  border-right-color: #ffacac !important;
  border-right: 15 solid #ffacac!important;
  border-left:0px!important;
}

.link-preview{
            width: 100%;
            display: flex;
            align-items: flex-start;
            padding: 15px 5px;
            border-bottom: 2px solid rgb(240, 240, 240);
        }
        .link-preview-img{
            width: 120px;
            max-height: 100%;
            object-fit: cover;
        }
        .link-preview-body{
            padding-left: 15px;
        }
        .link-preview-title{
            margin: 0;
            padding: 0;
        }
        .link-preview-title a{
            text-decoration: none;
            color: black;
        }

        .link-preview-description{
            margin: 0;
            padding: 0;
            margin-top: 10px;
            color: #555;
        }

        .link-preview-domain{
            margin: 0;
            margin-bottom: 5px;
        }
        .link-preview-domain a{
            text-decoration: none;
            color: #777;
            transition: .3s;
        }
        .link-preview-domain a:hover{
            color: black;
        }

</style>

<?php

$timezone = "";
$pair = @$pair ? $pair : false;
$nbCol = @$nbCol ? $nbCol : 2;
$abuseMax=5;
$classLi="";
if(@$inline && $inline){
  if($nbCol == 2)
    $classLi="col-xs-12 col-sm-6 col-md-6";
  if($nbCol > 2){
    $viewMode="block";
    $classLi="col-xs-12 col-sm-6 col-md-4";
  }
}
foreach($news as $key => $media){
  $class = $pair || ($nbCol == 1) ? "timeline-inverted" : "";
	$pair = !$pair;

  if(isset($media["type"]) && $media["type"] === "activitypub"){
?>
  <li class="<?= $class; ?> <?= $classLi ?> list-news" id="<?= @$media["type"]; ?><?= $key; ?>">
    <div class="timeline-panel">
      <?= $this->renderPartial('news.views.co.timelinePanelActivitypub', ["key"=>$key, "media"=>$media]) ?>
    </div>
  </li>
<?php }else{
  // Author name and thumb
  if(@$media["targetIsAuthor"] || $media["type"]=="activityStream"){
      if(@$media["target"]["profilThumbImageUrl"] && $media["target"]["profilThumbImageUrl"] != "")
        $thumbAuthor = Yii::app()->createUrl('/'.$media["target"]["profilThumbImageUrl"]);
      else
        $thumbAuthor = $this->module->assetsUrl."/images/thumb/default_".$media["target"]["type"].".png";
      $nameAuthor=$media["target"]["name"];
      $authorType=$media["target"]["type"];
      $authorId=$media["target"]["id"];
  } else{
     $thumbAuthor =  @$media['author']['profilThumbImageUrl'] ?
                  Yii::app()->createUrl('/'.@$media['author']['profilThumbImageUrl'])
                  : $this->module->assetsUrl."/images/thumb/default_citoyens.png";
      $nameAuthor=@$media["author"]["name"];
      $authorType=Person::COLLECTION;
      $authorId=@$media["author"]["id"];
  }
  //$srcMainImg = "";
  //if(@$media["media"]["images"] && $media["media"]["type"] != "gallery_images")
    //$srcMainImg = Yii::app()->createUrl("upload/".
      //                                  Yii::app()->controller->module->id."/".
        //                                $media["media"]["images"][0]["folder"].'/'.
          //                              $media["media"]["images"][0]["name"]);

  //if(@$media["imageBackground"])
    //$srcMainImg = Yii::app()->createUrl($media["imageBackground"]);
?>
  <li class="<?php echo $class; ?> <?php echo $classLi ?> list-news"
      id="<?php echo @$media["type"]; ?><?php echo $key; ?>">
    <div class="timeline-badge primary">
      <a><i class="glyphicon glyphicon-record" rel="tooltip"></i></a>
    </div>
    <div class="timeline-panel"
         id="nbAbuse<?php echo @$media["reportAbuseCount"]; ?>">
         <?php
            $abuseMax = $this->appConfig["nbReportCoModeration"];

            if( @$media["reportAbuseCount"] >= $abuseMax){ ?>
          <!-- <h6 class="pull-left">
              <small class="pull-left margin-left-10 letter-orange">
                <i class="fa fa-flag"></i> Ce contenu a été signalé <?php echo @$media["reportAbuseCount"]; ?> fois !
                <?php if(@$media["reportAbuseCount"] < $abuseMax){ ?>
                  <br><b>participez à la modération en signalant le contenu qui vous semble innaproprié</b>
                <?php }else{ ?>
                  <br><b>participez à la modération en votant</b>
                <?php } ?>
              </small>
           </h6>
           <?php if(@$media["reportAbuseCount"] >= $abuseMax && isset(Yii::app()->session["userId"])){ ?>
           <button class="btn btn-link bg-orange pull-right margin-right-10 margin-top-10 btn-start-moderation"
                   data-newsid="<?php echo @$media["_id"]; ?>"
                   data-toggle="modal" data-target="#modal-moderation">
              <i class="fa fa-balance-scale"></i> Modération
          </button>-->
          <?php } ?>
         <?php } ?>
      <?php
        echo $this->renderPartial('news.views.co.timelinePanel',
                    array(  "key"=>$key,
                            "media"=>$media,
                            "objectUUID" => @$media["objectUUID"],
                            "authorType"=>$authorType,
                            "nameAuthor"=>@$nameAuthor,
                            "authorId"=>$authorId,
                            "contextId"=>@$contextParentId,
                            "timezone"=>$timezone,
                            "thumbAuthor"=>@$thumbAuthor,
                            "canManageNews"=> @$canManageNews,
                            "timelineHtml"=> @$timelineHtml,
                            "fediTags" => @$fediTags
                        )
                    );
      ?>
      <?php
      // if( isset(Yii::app()->session["userId"]) &&
      //           (!isset($timelineHtml) || !isset($timelineHtml["footer"]))) {
                  ?>
      <div class="timeline-footer pull-left col-md-12 col-sm-12 col-xs-12 padding-top-5">
          <div class="col-xs-12 pull-left padding-5" id="footer-news-<?php echo @$media["_id"]; ?>"></div>
          <div class="col-md-12 col-sm-12 col-xs-12 no-padding pull-left margin-top-10" data-objectid="<?php echo @$media["objectUUID"]; ?>" id="commentContent<?php echo @$media["_id"]; ?>"></div>
      </div>
      <?php //} ?>
    </div>
  </li>
<?php }} ?>
<?php if(!(@$isFirst == true) && isset($limitDate) && isset($limitDate["created"])){ ?>
    <input class="dateLimit" type="hidden" value="<?php echo (is_string($limitDate["created"])) ? $limitDate["created"] : $limitDate["created"]->sec; ?>">
<?php } ?>

<?php
  if(isset($removeNews)){ ?>
    <input class="relaunchSearch" type="hidden" value="true">
  <?php }else{
    if(isset($isFirst) == true && sizeof($news)==0){  ?>
      <li id='noMoreNews' class='text-left padding-15 letter-blue shadow2'>
        <i class='fa fa-ban'></i>
        <?php echo Yii::t("common", "No news in this timeline") ?>
      </li>
  <?php }else if(isset($isFirstSearch) && $isFirstSearch && sizeof($news)== 0){?>
     <li id='noMoreNews' class='text-left padding-15 letter-blue shadow2'>
     <i class='fa fa-ban'></i>
     <?php echo Yii::t("common", "No news in this timeline") ?>
   </li>
   <?php }else if(sizeof($news)==0 && isset($actionController) != "save"){
     echo "<li id='noMoreNews' class='text-left letter-blue shadow2'><i class='fa fa-ban'></i> ".Yii::t("common", "No more news")."</li>";
} } ?>
<script type="text/javascript">
  var news=<?php echo json_encode($news) ?>;
  //var removeAll=<?php echo (!isset($removeNews)) ? false : true ?>;
  var canPostNews = <?php echo json_encode(@$canPostNews) ?>;
  var canManageNews = <?php echo json_encode(@$canManageNews) ?>;
  var actionController = <?php echo json_encode(@$actionController) ?>;
  var idSession = "<?php echo Yii::app()->session["userId"] ?>";
  var uploadUrl = "<?php echo Yii::app()->params['uploadUrl'] ?>";
  var docType="<?php echo Document::DOC_TYPE_IMAGE; ?>";
  var months = ["<?php echo Yii::t('common','january') ?>", "<?php echo Yii::t('common','febuary') ?>", "<?php echo Yii::t('common','march') ?>", "<?php echo Yii::t('common','april') ?>", "<?php echo Yii::t('common','may') ?>", "<?php echo Yii::t('common','june') ?>", "<?php echo Yii::t('common','july') ?>", "<?php echo Yii::t('common','august') ?>", "<?php echo Yii::t('common','september') ?>", "<?php echo Yii::t('common','october') ?>", "<?php echo Yii::t('common','november') ?>", "<?php echo Yii::t('common','december') ?>"];
  var contentKey = "<?php echo Document::IMG_SLIDER; ?>";
  var scrollEnd=<?php echo json_encode(@$endStream) ?>;
  //var scroll=<?php echo json_encode(@$scroll) ?>;
  //var nbCol = <?php echo json_encode(@$nbCol); ?>;
  //var viewMode = "<?php echo @$viewMode ?>";
  jQuery(document).ready(function() {
    $.each(news, function(e,v){
      //do not apply the default behavior if it is activitypub type news
      if(v.type == "activitypub")
        return true;

      updateNews[e]= v;
      if(typeof v._id.$id != "undefined")
      if($("#news-list .newsActivityStream"+v._id.$id).length>0)
          $("#news-list #news"+v._id.$id).remove();

      if(typeof v.object != "undefined"){
        if($("#news-list .newsActivityStream"+v.object.id).length>0)
          $("#news-list #news"+v.object.id).remove();

        $("#news-list .newsActivityStream"+v.object.id).each(function(b, ob){
          if(b>0) {
            var parent = $(ob).parent().parent().parent();
            parent.remove();
          }
        });
        if(v.object.type != "news"){
          renderType=(v.object.collection=="proposal") ? "directory.coopPanelHtml" :"directory.elementPanelHtml";
          var html = directory.showResultsDirectoryHtml(new Array(v.object), renderType);
          $(".newsActivityStream"+v.object.id + "[data-type='"+v.object.type+"']").html(html);
        }
      }
    });

    $.each(news, function(e,v){
      //do not apply the default behavior if it is activitypub type news
      if(v.type == "activitypub")
        return true;

      tags = "",
      scopes = "",
      tagsClass = "",
      scopeClass = "",
      markdownActive = v.markdownActive,
      markdownConverterOptions = {
        tasklists:true,
        tables:true,
        requireSpaceBeforeHeadingText:true,
        simplifiedAutoLink:true,
        openLinksInNewWindow:true

      };

      if(actionController=="save"){
        if(nbCol>1)
          $("#"+v.type+e).parent().find(".list-news:eq(1)").addClass("addMargin");
        if(!$("#"+v.type+e).parent().find(".list-news:eq(1)").hasClass("timeline-inverted") || nbCol==1)
          $("#"+v.type+e).addClass("timeline-inverted");
      }

      // CSS DESIGN NEWS ORGANIZATION
      var currentOffset=$("#"+v.type+e).offset();
      var prevOffset=$("#"+v.type+e).prevAll(".list-news").offset();
      if(typeof prevOffset != "undefined"){
        if(currentOffset.top>=(prevOffset.top-20) && currentOffset.top<=(prevOffset.top+20))
           $("#"+v.type+e).addClass("addMargin");
      }
      //if(!$("#"+v.type+e).prevAll(".list-news").hasClass("timeline-inverted") && !$("#"+v.type+e).hasClass("timeline-inverted"))
        //$("#"+v.type+e).addClass("timeline-inverted");
      //if($("#"+v.type+e).prevAll(".list-news").hasClass("timeline-inverted") && $("#"+v.type+e).hasClass("timeline-inverted"))
        //$("#"+v.type+e).removeClass("timeline-inverted");

      if(actionController=="save")
        initCommentsTools(new Array(v));
      if("undefined" != typeof v.text){
        textHtml="";
        textNews=v.text;
        if(v.text.length > 0 && !markdownActive)
          textNews=checkAndCutLongString(v.text,500,v._id.$id,"showmorenews",true);
          //Check if @mentions return text with link
        if(typeof(v.mentions) != "undefined")
          textNews = mentionsInit.addMentionInText(textNews,v.mentions);
        var textContent = $.displayCoEmoji(textNews);
        textContent = markdownActive ?
                      dataHelper.markdownToHtml(textContent, markdownConverterOptions):
                      linkify(textContent);
        textHtml = `<span class="timeline_text no-padding text-black ${markdownActive?"markdown-body":""}">${textContent}</span>`;

        $("#newsContent"+e).html(textHtml);

        $(".btn-showmorenews").off().click(function(){
          var newsid = $(this).data("id");
          if($("#newsContent"+newsid+" .timeline_text span.endtext").hasClass("hidden")){
              $("#newsContent"+newsid+" .timeline_text span.endtext").removeClass("hidden");
              $("#newsContent"+newsid+" .timeline_text span.ppp").addClass("hidden");
              $(this).html("<?php echo Yii::t('common','Read less') ?>").parent().prepend("<br>");
          }else{
              $("#newsContent"+newsid+" .timeline_text span.endtext").addClass("hidden");
              $("#newsContent"+newsid+" .timeline_text span.ppp").removeClass("hidden");
              $(this).html("<?php echo Yii::t('common','Read more') ?>").parent().find("br").remove();
          }
        });
      }
      if("undefined" != typeof v.verb && v.verb=="share"){
        if(typeof v.object != "undefined" && typeof v.object.media != "undefined"){
          newsObj.createAndInsertMediaRender(v.object.id, v.object);
        }
      }
      if("undefined" != typeof v.media){
        newsObj.createAndInsertMediaRender(e, v);
      }
      if("undefined" != typeof v.mediaImg){
        newsObj.createAndInsertMediaRender(e, v, "image");
      }
      if("undefined" != typeof v.mediaFile){
        newsObj.createAndInsertMediaRender(e, v, "file");
      }

      showNewLinkPreview(v);

      coInterface.bindLBHLinks();
    });

    $(".btn-start-moderation").off().click(function(){
      var newsid = $(this).data("newsid");
      uiModeration.getNewsToModerate(newsid);
    });
    <?php
    //if(isset(Yii::app()->session["userId"])) {
      ?>
      initCommentsTools(news, "news");
    <?php
  //}
  ?>
    $(function() {
      if(typeof AddReadMoreNews == "function"){
        AddReadMoreNews();
      }
    });
    directory.bindBtnElement();
  });
</script>
