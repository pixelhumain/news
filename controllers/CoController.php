<?php

namespace PixelHumain\PixelHumain\modules\news\controllers;

use Authorisation;
use CommunecterController;
use Yii;
use ExtractProcessAction;
use ExtractFileAction;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
		Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
		Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

		// si la requête est de type OPTIONS, on retourne une réponse 200
		if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
			Yii::$app->response->setStatusCode(200);
			return Yii::$app->response->send();
		}
        $headers = getallheaders();
		if( isset($headers["X-Auth-Token"]) && isset($headers["X-User-Id"]) && isset($headers["X-Auth-Name"]) && Authorisation::isMeteorConnected( $headers["X-Auth-Token"], $headers["X-User-Id"], $headers["X-Auth-Name"] ) ){
      		$prepareData = false;
		} else if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
			Authorisation::isJwtconnected($matches[1]);
		}
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'index'   => \PixelHumain\PixelHumain\modules\news\controllers\actions\IndexAction::class,
	        'get'   => \PixelHumain\PixelHumain\modules\news\controllers\actions\GetAction::class,
	        'moderate'      => \PixelHumain\PixelHumain\modules\news\controllers\actions\ModerateAction::class,
	        'save'     		=> \PixelHumain\PixelHumain\modules\news\controllers\actions\SaveAction::class,
	        'delete'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\DeleteAction::class,
			'addipspam'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\AddIpSpamAction::class,
			'getinfobeforeaddtospamelement'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\GetInfoBeforeAddSpamElementAction::class,
			'getinfobeforedeletespamelement'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\GetInfoBeforeDeleteSpamElementAction::class,
			'deletespam'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\DeleteSpamAction::class,
			'nospam'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\NoSpamAction::class,
	        'update'   		=> \PixelHumain\PixelHumain\modules\news\controllers\actions\UpdateAction::class,
			'share' 		=> \PixelHumain\PixelHumain\modules\news\controllers\actions\ShareAction::class,
	        'extractprocess' => array (
	            'class'   	=> ExtractProcessAction::class,
	            'options' 	=> array(
	                // Tmp dir to store cached resized images 
	                'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets/',	 
	                // Web root dir to search images from
	                'base_dir'    => Yii::getPathOfAlias('webroot') . '/',
	            )
	        ),
	        'extractfile' => array (
	            'class'   	=> ExtractFileAction::class,
	            'options' 	=> array(
	                // Tmp dir to store cached resized images 
	                'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets/',	 
	                // Web root dir to search images from
	                'base_dir'    => Yii::getPathOfAlias('webroot') . '/',
	            )
			),
			'getbyids' => \PixelHumain\PixelHumain\modules\news\controllers\actions\GetByIdsAction::class,
			'getcommunitynewsletter' => \PixelHumain\PixelHumain\modules\news\controllers\actions\GetCommunityNewsLetterAction::class,
			"sendnewsletter" => \PixelHumain\PixelHumain\modules\news\controllers\actions\SendNewsLetterAction::class
	    );
	}
}
