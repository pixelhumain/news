<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use Exception;
use News;
use Rest;

class GetByIdsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        try{
            $ids = $_POST["ids"];
            return Rest::json(News::getByIds($ids));
        }catch(Exception $e){
            return Rest::json([]);
        }
    }
}