<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use PixelHumain\PixelHumain\modules\news\models\UrlExtractor;
use Rest;

class GetPageContentAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	if(isset($_POST['url'])){
			$tags = UrlExtractor::extract($_POST['url']);
			return Rest::json($tags);
		}else{
			return [];
		}
    }
}