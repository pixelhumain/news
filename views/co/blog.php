<?php 

$hashTagsExcluded=array("chiffre","citation");

$carousel_indicators_color="#eeeeee";

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    //MARKDOWN
    '/plugins/to-markdown/to-markdown.js',              
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

// $chiffre=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("chiffre"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);

// $citation=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("citation"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);
    
// $evenements=Event::getEventByWhereSortAndLimit(array("startDate"=>array( '$gte' => new MongoDate(microtime(true))), "source.key"=>"journalInsoumisChambery"),array("startDate" => 1),3,0);

// if(count($evenements)<3){
//   $evenements=array_merge($evenements,Event::getEventByWhereSortAndLimit(array("startDate" => array( '$lte' => new MongoDate(microtime(true))), "source.key"=>"journalInsoumisChambery"),array("startDate" => -1),3-count($evenements),0));
// }

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true","notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 3, 0);
//var_dump($articles_une);


// $articles=Poi::getPoiByWhereSortAndLimit(array('$and'=>array(array("notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"))),array("updated"=>-1), 10, 0); 
//var_dump($this->costum["contextId"]);
$costumId=(isset($this->costum) && isset($this->costum["contextId"])) ? $this->costum["contextId"] : "journalInsoumisChambery" ;
//var_dump($costumId);
 $articles=PHDB::findAndSort(News::COLLECTION,array("target.id"=>$costumId),array("created"=>-1)); 
//var_dump($articles);
//var_dump($articles);exit;

$nb_articles=0;
foreach ($articles as $key => $value) {
  if($nb_articles>7){
    unset($articles[$key]);
  } else {
    foreach ($articles_une as $key_une => $value_une) {
      if(strcmp($key, $key_une)===0){
        unset($articles[$key]);
        break;
      }
    }
    if(isset($articles[$key])){
      $nb_articles=$nb_articles+1;
    }
  }
}

?>

<style type="text/css">
  #a2k_page .searchEntityContainer{
      overflow: auto;
      min-height:100px !important;
      max-height:600px !important;
      padding : 0px;
    }
    #a2k_page .searchEntity {
      overflow: auto;
      margin-bottom: 20px;
      box-shadow: none;
      /*border-right: 1px solid #0098B6;
      border-bottom: 1px solid #0098B6;*/
    }
    #a2k_page .searchEntity .btn-link-content{
      text-align: right;
    }
    #a2k_page .searchEntity .btn-link-content .btn-link{
      color: #c9462c;
      border-color: #c9462c;
      font-size: 12px;
      padding: 4px 7px;
      min-width: fit-content;
    }
    #a2k_page .searchEntity .btn-link-content .btn-link:hover{
      background: #c9462c;
      color: white;
    }
    .searchEntity .entityRight .entityName{
      white-space: normal;
      font-size: 16px !important;
    }
    #a2k_page .container-img-profil{
      display: block; 
      min-height:0px; 
      max-height: 400px;   
    }

    #a2k_page .dateUpdated{
      display:none;
    }

    .bodySearchContainer{
      max-width: 1200px;
      margin:auto;
      float:none;
    }

    .searchEntity .entityCenter{
      display: none;
    }

    .searchEntity .actionSocialBtn{
      display : none;
    }

    .searchEntity .dateUpdated{
      /*display: none;*/
    }
    .searchEntity .followBtn{
      display: none;
    }
    .searchEntity .btn-share{
      /*display: none;*/
    }
    .searchEntity .co3BtnShare{
      padding-bottom:5px;
      display: none !important;
    }

    .searchEntity .entityRight .entityDescription{
      font-size: 14px !important;
    }

    .searchEntity .entityRight .entityLocality{
      font-size: 14px !important;
    }

    .searchEntity .tagsContainer .badge{
      font-size: 14px !important;
    }

    
  #a2k_main_conteneur{
      display:block;
      overflow: auto;
    }


   .friend{
      display:flex;
      overflow: auto;
      height:100px !important;
   }

   .friend img{
      margin:auto;
      max-width:100%;
      max-height: 100px;
   }

    #a2k_left-column{
      display:block;
      overflow: auto;
    }
    #a2k_right-column{
      display:block;
      overflow: auto;
    }
    .top-separator{
      border-top: 2px solid #eeeeee;
    }
    .left-separator{
      border-left: 2px solid #eeeeee;
    }
    .right-separator{
      border-right: 2px solid #eeeeee;
    }
    .bloc_rubrique{
      /*margin-top: 10px;
      margin-bottom: 10px;
      display:block;*/
      overflow: auto;
    }
    .titre_rubrique{
      font-size:20px;
      margin-top: 0px;
      margin-bottom: 20px;
      display:block;
      overflow: auto;
    }
    .contenu_rubrique{
      display:block;
      overflow: auto;
    }

    .badge_article{       
        border-radius: 20px;
        padding: 20px 10px;
        background-color: #0098B6 !important;
        width : 100%;
    }

    .badge_article_titre{
        font-size: 20px;
        color: white !important;
        width : 100%;
        text-transform: uppercase;
        text-align: center;
    }

    #chiffre_container .badge_article_titre{
        font-size: 36px;
    }

    .badge_article_description{
        font-size: 12px;
        color: white !important;
        width : 100%;
        text-align: center;
    }
    .open-xs-menu{
      color: white !important;
    }

    #openModal.portfolio-modal.modal,#ajax-modal.portfolio-modal.modal {
        background-color: rgba(0,0,0,0.5) !important;
        padding: 50px !important;
        border-radius: 10px !important;
        min-height: unset !important;
        z-index: 200000;
        /* padding-left: 0px !important; */
        top: 0px !important;
        right: 0px;
        left: 0px !important;
    }
    .mh150{
    max-height:150px;
    }
    .h150{
    height:150px;
    }
    .mw1000{
      max-width: 1200px;
    }
    .w-100{
      width:100%;
    }
    .mx-auto{
      margin-left: auto;
      margin-right: auto;
    }
    .w-80{
      width:80%;
    }
    .w-50{
      width:50%;
    }
    .mx-auto{
      margin-left: auto;
      margin-right: auto;
    }
    .my-0{
      margin-top: 0 !important; 
      margin-bottom: 0 !important;
    }
    .px-auto{
      padding-right: 0;
      padding-left: 0;
    }
    .p-20{
      padding:20px;
    }
    .p-30{
      padding:30px;
    }
    .pl-0{
      padding-left:0px;
    }
    .pr-0{
      padding-right:0px;
    }

</style>

<div id="a2k_page" class="w-100 mx-auto">

  <div id="a2k_main_conteneur" class="w-100">
    <div class="w-100 mw1000 mx-auto">
      <div id="a2k_left-column" class="col-sm-12 col-md-8 top-separator right-separator pl-0 pr-0">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             A la une
          </div>         
          <div id="une_container" class="contenu_rubrique w-100">  
            <?php
            echo $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"articleCarousel","nbItem"=>count($articles_une),"carousel_indicators_color"=>"#eeeeee")); 
            ?>
          </div>       
        </div>
        <!-- <div class="bloc_rubrique w-100 top-separator my-0">
          <div class="col-xs-12 col-sm-6 right-separator p-30">
            <div class="titre_rubrique w-100 b maj">
               Chiffre du jour
            </div>
            <div id="chiffre_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 left-separator p-30" style="margin-left : -2px;">
            <div class="titre_rubrique w-100 b maj">
               Citation du jour
            </div>
            <div id="citation_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
        </div>
        <div class="bloc_rubrique w-100 top-separator p-30">
          <div class="titre_rubrique w-100 b maj">
             Agenda
          </div>
          <div id="agenda_container" class="contenu_rubrique w-100">
            <?php
            // echo $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"agendaCarousel","nbItem"=>count($evenements),"carousel_indicators_color"=>"#eeeeee")); 
            ?>
          </div>
        </div> -->
      </div>
      <div id="a2k_right-column" class="col-sm-12 col-md-4 top-separator left-separator pl-0 pr-0" style="margin-left : -2px;">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             Derniers articles
          </div>
          <div id="last_container" class="contenu_rubrique w-100">
          </div>
        </div>
      </div>
      <!-- <div id="a2k_full-column" class="col-xs-12 top-separator p-30">
        <div class="titre_rubrique w-100 b maj">
          Organisations amies
        </div>
        <div id="friend_container" class="contenu_rubrique mx-auto">
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://franceinsoumise.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fi.png" alt="La France Insoumise">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.heuredupeuple.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-hdp.png" alt="L'Heure du Peuple">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://avenirencommun.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-aec.png" alt="L'Avenir en Commun">
          </div>        
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.lepartidegauche.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-pdg.png" alt="Le Parti de Gauche">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.fakirpresse.info','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fp.png" alt="Fakir Presse">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.amd-savoie.com','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-amd-savoie.jpg" alt="Les Amis du Monde Diplomatique Savoie">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('https://www.facebook.com/attac73/','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-attac-savoie.jpg" alt="Attac Savoie">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('https://www.facebook.com/emilie.narvaline.1','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-emilie-marche.png" alt="Emilie Marche">
          </div>
        </div>
      </div> --> 
    </div>
  </div>
  <!-- <div id="a2k_footer" class="w-100">
      <a href="mailto:contact@journal-insoumis-chambery.com" style="color:#ffffff;">Contact</a> - <span id="mentionsLegalesBtn" style="cursor:pointer;" onclick="alert('Responsable de la publication : LARIGUET Georges');">Mentions Légales</span>
      <script>
        $("#mentionsLegalesBtn").mouseenter(function(){$(this).css('text-decoration','underline');}).mouseleave(function(){$(this).css('text-decoration','none');});
      </script>
  </div> -->
</div>

<script type="text/javascript">
var A2K_articles_une = null;
var A2K_articles = null;
var A2K_evenements = null;
var A2K_chiffre = null;
var A2K_citation = null;

function initWelcome(){
  let socialBarConfig={btnList:[{type:'twitter'},{type:'facebook'},{type:'co'}],btnSize:16};

  A2K_articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
  A2K_articles = new CO3_Article(null,<?php echo json_encode($articles); ?>);

  $("#articleCarousel .carousel-inner").html(A2K_articles_une.SetCarousable().SetSocialBarOn(socialBarConfig).SetDisplayImg("full").RenderHtml());
  $("#last_container").html(A2K_articles.SetSocialBarOn(socialBarConfig).SetDisplayImg("none").RenderHtml());
  
  // $("#chiffre_container").html(A2K_chiffre.RenderHtml());
  // $("#citation_container").html(A2K_citation.RenderHtml());

  // $("#agendaCarousel .carousel-inner").html(A2K_evenements.SetCarousable().SetSocialBarOn(socialBarConfig).RenderHtml());
      
  jsOnLoad();        
  setTimeout(function() {jsOnLoad();}, 1000); 
  setTimeout(function() {jsOnLoad();}, 5000);
}

function resizeCarousels(){
  CO3_Article.forceListImRatio("#articleCarousel .container-img-profil",16/9,CO3_Article.carouselGetActiveWidth("#articleCarousel"));
  // CO3_Event.forceListImRatio("#agendaCarousel .container-img-profil",1,CO3_Event.carouselGetActiveWidth("#agendaCarousel"));
  
}

jQuery(window).resize(function() {
  resizeCarousels();  
});

function jsOnLoad(){
  coInterface.bindLBHLinks();
  directory.bindMediaSharingElt(); 
  resizeCarousels()
  start_articleCarousel();
  // start_agendaCarousel();
}

function lazyWelcome(time){
  if(typeof CO3_Article != "undefined")
    initWelcome();
  else
    setTimeout(function(){
      lazyWelcome(time+200)
    }, time);
} 

jQuery(document).ready(function() {
  //setTitle("Journal de la FI Chambéry");
  lazyWelcome(0);
  // TODO arriver à choper le window on load !!!!
});

</script>


