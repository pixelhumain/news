<?php
namespace PixelHumain\PixelHumain\modules\news\models;

use CacheHelper;
use DOMDocument;

class UrlExtractor{
    public static function extract($url){
       	if($tags = CacheHelper::get(base64_encode($url))){
			return $tags;
		}else{
			$tags = self::extract_tags($url);
			CacheHelper::set(base64_encode($url), $tags);
			return $tags;
		}
    }

    private static function file_get_contents_curl($url)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	private static function extract_tags($url){
        $pageContents = self::file_get_contents_curl($url);
        
		$tags = [];
		$doc = new DOMDocument();
		@$doc->loadHTML(mb_convert_encoding($pageContents, 'HTML-ENTITIES', 'UTF-8'));

		//get title
		$titleNodes = $doc->getElementsByTagName('title');
		$tags['title'] = sizeof($titleNodes)>0?$titleNodes[0]->nodeValue:"";

		//get metas
		$metaNodes = $doc->getElementsByTagName('meta');
		$tags["meta"] = [];
		foreach($metaNodes as $meta){
			if($meta->getAttribute("name") && $meta->getAttribute("content"))
				$tags["meta"][$meta->getAttribute("name")] = $meta->getAttribute("content");
			else if($meta->getAttribute("property") && $meta->getAttribute("content"))
				$tags["meta"][$meta->getAttribute("property")] = $meta->getAttribute("content");
		}

        //get links
        $tags["link"] = [];
        $headNodes = $doc->getElementsByTagName('head');
        foreach($headNodes as $head){
            $linkNodes = $head->getElementsByTagName("link");
            foreach($linkNodes as $link){
                if($link->getAttribute("rel") && $link->getAttribute("href"))
                    $tags["link"][$link->getAttribute("rel")] = self::getAbsoluteUrl($url, $link->getAttribute("href"));
            }
        }
		
		return $tags;
	}

    private static function getAbsoluteUrl($baseUrl, $url){
        if(preg_match("~^(?:f|ht)tps?://~i", $url))
            return $url;

        return $baseUrl.$url;
    }
}