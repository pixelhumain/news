<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use Element;
use Person;
use PHDB;
use Rest;

class GetCommunityNewsLetterAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($type, $id) {
        $community = Element::getCommunityByTypeAndId($type, $id, Person::COLLECTION);
        $persons = [];

        foreach($community as $id => $item){
            $persons[$id] = Person::getById($id, false);
        }

        return Rest::json($persons); 
    }

}