
var newsObj = {
	config:{
		type : "city",
		isLive : true,
	},
	currentlyLoadingData : false,
	init : function () {
		scrollEnd = false;
		var dateLimit=0;
		newsObj.loadStream(newsObj.config);
		if(newsObj.config.scroll && newsObj.config.scroll!=="false"){
			scrollTarget=(typeof newsObj.config.scrollTarget != "undefined" && notEmpty(newsObj.config.scrollTarget)) ? newsObj.config.scrollTarget : window;
	        $(scrollTarget).on("scroll",function(e){
	        	e.preventDefault(); 
			    if(!newsObj.currentlyLoadingData && !scrollEnd && colNotifOpen){
			          var heightWindow = $("html").height() - $("body").height();
			          if( $(this).scrollTop() >= heightWindow - 1000){
			            newsObj.loadStream(newsObj.config);
			          }
			    }
			});
		}else if(newsObj.config.click){
			$(newsObj.config.clickEvent).click(function(){
				newsObj.loadStream(newsObj.config);
			});
		}
		//loadingData = false;
		newsObj.initForm();
		newsObj.bindEvent();
        //$('#tags').select2({tags:tagsNews});
        
        $('#tags').select2({
            minimumInputLength: 3,
            tags: true,
            multiple: true,
            createSearchChoice: function (term, data) {
                 if (!data.length)
                    return {
                        id: term,
                        text: term
                    };
            },
            ajax: {
                url: baseUrl+'/api/tags/search',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        q: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.tag,
                                id: item.tag
                            }
                        })
                    };
                }
            }
        });
  		$("#tags").select2('val', "");
	},
	loadLive : function(url, key, settings){
	     params={
	        nbCol: (typeof nbCol != "undefined") ? nbCol : 2,
	        inline : (typeof  inline != "undefined") ? inline : false,
	        search : {}
	    };
	    if(notNull(key) && notNull(settings) && typeof settings[key] != "undefined"){
	        if(typeof settings[key].tags != "undefined")
	            params.search.searchTags=settings[key].tags;
	        if(typeof settings[key].locality != "undefined")
	            params.search.locality=settings[key].locality;
	        if(typeof settings[key].type != "undefined")
	            params.search.type=settings[key].type;
	    }
	   // var url = "news/co/index/type/citoyens/id/"+userId+"/isLive/true";
	   
	    setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
	    	var domLive=($("#newsstream").length > 0) ? "#newsstream" : "#central-container";
	        showLoader(domLive);
	        ajaxPost(domLive, baseUrl+'/'+url, 
	            params,
	            function(){ 
	                //loadLiveNow();
	            });
	    }, 700);
	},
	loadStream : function(params){ 
		//mylog.log("loadStream", params);
		newsObj.currentlyLoadingData = true;
		if(typeof dateLimit == "undefined") dateLimit = 0;
		var url = "news/co/get";
		var scrollProcess=(typeof params.scroll != "undefined") ? params.scroll : true; 
		var dataSearch= (notNull(params.search) && params.search) ? searchInterface.constructObjectAndUrl(): {};
		if(notNull(params.type)) url+="/type/"+params.type;
		if(notNull(params.id)) url+="/id/"+params.id;
		if(notNull(params.isLive) && params.isLive) url+="/isLive/true";
		if(notNull(params.source) && notEmpty(params.source)) url+="/source/"+params.source;
		url+="/date/"+dateLimit;

		var dataNews = { 
			indexStep:params.indexStep,
			nbCol:params.nbCol,
			timelineHtml:params.timelineHtml,
			scroll:scrollProcess,
			inline:params.inline,
			search: dataSearch 
		}

		if(typeof params.typeNews != "undefined" && params.typeNews != null)
			dataNews.typeNews = params.typeNews;

		if(typeof params.searchTags != "undefined" && params.searchTags != null)
			dataNews.search["searchTags"] = params.searchTags;

		ajaxPost(
	        null,
	        baseUrl+"/" + url,
	        dataNews,
	        function(data){ 
	        	if(data){ 
                	$(".container-live").find(".loader").remove();
                	$(params.containerTreeNews).append(data);
                	if($("#noMoreNews").length<=0 && scrollProcess)
						$(".container-live").append(loading);
                	//bindTags();
					newsObj.currentlyLoadingData = false;

					const arr = document.querySelectorAll('img.lzy_img')
					arr.forEach((v) => {
						imageObserver.observe(v);
					});
					
				}
				$(".stream-processing").hide();
	        },
	        function(xhr, status, error){
	            newsObj.currentlyLoadingData = false;
	        }
        );
	},
	bindEvent : function(){
		var separator, anchor;
		$("#get_url").elastic();
		$("#get_url").change(function(){
			if($(this).val()!="") $(".form-create-news-container .form-actions .writesomethingplease").remove();
		});
		$(".scopeShare").click(function() {
			replaceText=$(this).find("h4").html();
			$("#btn-toogle-dropdown-scope").html(replaceText+' <i class="fa fa-caret-down" style="font-size:inherit;"></i>');
			scopeChange=$(this).data("value");
			$("input[name='scope']").val(scopeChange);
			if(scopeChange == "public"){
				if($("#scopeListContainerForm").html()=="") getScopeNewsHtml();
				$("#scopeListContainerForm").show(700);
		  	}else
		  		$("#scopeListContainerForm").hide(700);
		});
		$(".targetIsAuthor").click(function() {
			//mylog.log(this);
			srcImg=$(this).find("img").attr("src");
			$("#btn-toogle-dropdown-targetIsAuthor").html('<img height=20 width=20 src="'+srcImg+'"/> <i class="fa fa-caret-down" style="font-size:inherit;"></i>');
			authorTargetChange=$(this).data("value");
			$("#authorIsTarget").val(authorTargetChange);
		});

		$('.newsShare').off().on("click",function(){
			toastr.info('TODO : SHARE this news Entry');
			//mylog.log("newsShare",$(this).data("id"));
			count = parseInt($(this).data("count"));
			$(this).data( "count" , count+1 );
			$(this).children(".label").html($(this).data("count")+" <i class='fa fa-retweet'></i>");
		});

		$(".videoSignal").click(function(){
			videoLink = $(this).find(".videoLink").val();
			iframe='<div class="embed-responsive embed-responsive-16by9">'+
				'<iframe src="'+videoLink+'" width="100%" height="" class="embed-responsive-item"></iframe></div>';
			$(this).parent().next().before(iframe);
			$(this).parent().remove();
		});
	},	
	initForm: function(){
		
		$(".uploadImageNews").click(function(event){
	       	if (!$(event.target).is('input')) {
	        	$(this).find("input").trigger('click');
	    	}
	  	});
	  	$(".uploadFileNews").click(function(event){
	       	if (!$(event.target).is('input')) {
	        	$(this).find("input").trigger('click');
	    	}
	  	});
	  	showFormBlock(false);
	  	$("#formCreateNewsTemp").removeClass('hidden');
		newsObj.initFormImages();
		processUrl.getMediaFromUrlContent(".get_url_input",".results",1); 
	  	mentionsInit.get("textarea.mention");
	  	//if(typeof tagQDJ != "undefined" && tagQDJ != null) //tag question du jour
	    //	$("#form-news #tags").select2("val",new Array(tagQDJ));
	    $('#form-news #tags').select2({
            minimumInputLength: 3,
            tags: true,
            multiple: true,
            createSearchChoice: function (term, data) {
                 if (!data.length)
                    return {
                        id: term,
                        text: term
                    };
            },
            ajax: {
                url: baseUrl+'/api/tags/search',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        q: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.tag,
                                id: item.tag
                            }
                        })
                    };
                }
            }
        });
  		$("#form-news #tags").select2('val', "");
	},
	initFormImages(){

		// SET FOR IMAGE COPY PASTE
        setupImageUploader();


		//SET FOR IMAGE 
		$("#photoAddNews").on('submit',(function(e) {
			e.preventDefault();
			if(contextParentType=="city" || contextParentType=="pixels"){
				contextParentType = "citoyens";
				contextParentId = idSession;
			}
			extraParamsAjax={
		        contentType: false,
				cache: false, 
				processData: false
		    };

			var fileInput = $("#addImage")[0];
			const formData = new FormData();

			if (fileInput.files && fileInput.files[0]) {
				var file = fileInput.files[0];
				var reader = new FileReader();
				reader.onload = function(e) {
					
					var img = new Image();
					img.onload = function() {
					
						EXIF.getData(img, function() {
							var orientation = EXIF.getTag(this, "Orientation");

							if (orientation === 8 || orientation === 6 || orientation === 5) {
							
								var canvas = document.createElement('canvas');
								var ctx = canvas.getContext('2d');
								
								canvas.width = img.width;
								canvas.height = img.height;
								ctx.drawImage(img, 0, 0);
							
								canvas.toBlob(function(blob) {
									const correctedFile = new File([blob], file.name, { type: file.type });
              						formData.append('newsImage', correctedFile);
									uploadAndSaveImage(formData, extraParamsAjax);
								}, file.type);

							} else {
								formData.append('newsImage', file);
								uploadAndSaveImage(formData, extraParamsAjax);
							}
						});
					};
					img.src = e.target.result;
				};

				reader.readAsDataURL(file);
			} else {
				console.error("No file selected.");
			}
		}));

		function uploadAndSaveImage(formData, extraParamsAjax) {
			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/document/uploadSave/dir/communecter/folder/"+contextParentType+"/ownerId/"+contextParentId+"/input/newsImage/docType/image/contentKey/slider",
				formData,
				function(data){ 
					if(debug)mylog.log(data);
					  if(data.result){
						toastr.success(data.msg);
						//setTimeout(function(){
						$(".imagesNews").last().val(data.id.$id);
						$(".imagesNews").last().attr("name","");
						$(".newImageAlbum").last().find("img").removeClass("grayscale");
						$(".newImageAlbum").last().find("i.noGoSaveNews").remove();
						$(".newImageAlbum").last().append("<a href='javascript:;' onclick='deleteImage(\""+data.id.$id+"\",\""+data.name+"\")'><i class='fa fa-times fa-x padding-5 text-white removeImage' id='deleteImg"+data.id.$id+"'></i></a>");
						//},200);
			
					} else{
						toastr.error(data.msg);
						if($("#resultsImg img").length>1)
							  $(".newImageAlbum").last().remove();
						  else{
							  $("#resultsImg").empty();
							  $("#resultsImg").hide();
						  }
					}
					$("#addImage").off();
					$(".form-create-news-container .form-actions .waitendofloading").remove();
				},null,null,
				extraParamsAjax
			);
		}


		$("#fileAddNews").on('submit',(function(e) {
			e.preventDefault();
			if(contextParentType=="city" || contextParentType=="pixels"){
				contextParentType = "citoyens";
				contextParentId = idSession;
			}
			extraParamsAjax={
		        contentType: false,
				cache: false, 
				processData: false
		    };
		    ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/document/uploadSave/dir/communecter/folder/"+contextParentType+"/ownerId/"+contextParentId+"/input/newsFile/docType/file",
		        new FormData(this),
		        function(data){ 
		        	if(debug)mylog.log(data);
		  		    if(data.result){
					    toastr.success(data.msg);
					    //setTimeout(function(){
					    $(".fileNews").last().val(data.id.$id);
					    $(".fileNews").last().attr("name","");
					    $(".newFileAlbum").last().find("i.noGoSaveNews").remove();
					    $(".newFileAlbum").last().append("<a href='javascript:;' onclick='newsObj.file.delete(\""+data.id.$id+"\",\""+data.name+"\")'><i class='fa fa-trash margin-left-10 fa-x padding-5 text-red removeFile' id='deleteFile"+data.id.$id+"'></i> <span class='text-red'>"+trad.delete+"</span></a>");
					    //},200);
			
					} else{
						toastr.error(data.msg);
						if($("#resultsFile .fileEntry").length>1)
					  		$(".newfileAlbum").last().remove();
					  	else{
					  		$("#resultsFile").empty();
					  		$("#resultsFile").hide();
					  	}
					}
					$("#addFile").off();
					$(".form-create-news-container .form-actions .waitendofloading").remove();
		        },null,null,
		        extraParamsAjax
	        );
		}));
	},
	file : {
		show : function(fileInput) {
			if($(".noGoSaveNews").length){
				toastr.info(trad.waitendofloading);
			}
			else if (fileInput.files[0].size > 5000000){
				toastr.info("Please reduce your image before to 5Mo");
			}
			else {
				countFile=$("#resultsFile .fileEntry").length;
				nbFile=countFile+1;
				htmlFile="";
				var files = fileInput.files;
				if(countFile==0){
					htmlFile = "<input type='hidden' class='type' value='gallery_files'/>";
					htmlFile += "<input type='hidden' class='count_files' value='"+nbFile+"'/>";
					$("#resultsFile").show();
				}
				else
					$(".count_file").val(nbFile);
				htmlFile+="<div class='newFileAlbum fileEntry'><i class='fa fa-spin fa-circle-o-notch fa-x text-green spinner-add-image noGoSaveNews'></i><span class='title-file'>"+fileInput.files[0].name+" - <i>"+fileInput.files[0].size+"</i></span>"+
				       	"<input type='hidden' class='fileNews docsId' name='goSaveNews' value=''/></div>";
				$("#resultsFile").append(htmlFile);
				$("#fileAddNews").submit();	  
			}
		},
		delete : function(id, name, communevent){
			var fileToDelete=id;
			if(communevent==true)
				path="communevent";
			else
				path="commuencter";
			ajaxPost(
		        null,
		        baseUrl+"/"+moduleId+"/document/delete/dir/"+moduleId+"/type/"+contextParentType+"/id/"+contextParentId,
		        {"name": name, "parentId": contextParentId, "parentType": contextParentType, "path" : path, "ids" : [fileToDelete]},
		        function(data){ 
		        	if(data.result){
						if(hideMsg!=true){
							$("#deletefile"+fileToDelete).parents().eq(1).remove();
							countFile=$("#resultsFile .fileEntry").length;
							if(countFile==0){
								$("#resultsFile").empty().hide();
							}
							else{
								$(".count_files").val(countFile);
							}
							toastr.success(data.msg);
						}
					}
					else{
						toastr.error(data.msg);
					}
		        }
		    );
		}
	},
	createAndInsertMediaRender: function(e, v, mediaType){
		if(!notNull(mediaType)){
			domContentMedia="result";
			if(typeof(v.media.type)=="undefined" || v.media.type=="url_content")
	         	media=processUrl.getMediaCommonHtml(v.media,"show",e);
	        else if (v.media.type=="gallery_images"){
	        	domContentMedia="resultImg";
	          	media=getMediaImages(v.media,e,v.author.id,v.target.name);
	        }
	        else if (v.media.type=="gallery_files"){
	        	domContentMedia="resultFile";
	          	media=getMediaFiles(v.media,e);
	        }
	        else if (v.media.type=="activityStream")
	          	media=directory.showResultsDirectoryHtml(new Array(v.media.object));
	        else if (v.media.type=="html" && typeof v.documentation != "undefined")
	          	media=v.media.content;
	        $("#"+domContentMedia+e).html(media);
	        if(v.media.type=="gallery_files" || v.media.type=="gallery_images")
	          	$("#"+domContentMedia+e).css({"padding": "10px !important"});
	        if(v.media.type=="html" && typeof v.documentation != "undefined")
	          	coInterface.bindButtonOpenForm();

	        $(".videoSignal").off().on("click",function(){
	          	videoLink = $(this).find(".videoLink").val();
	         	iframe='<div class="embed-responsive embed-responsive-16by9 margin-bottom-5" style="background-color:black;">'+
	            	'<iframe src="'+videoLink+'" width="100%" height="" class="embed-responsive-item"></iframe></div>';
	          	$(this).parent().next().removeClass("col-xs-8").addClass("col-xs-12").before(iframe);
	          	//$(this).parent().next();
	          	$(this).parent().remove();
	        });
	    }else if(mediaType=="image"){
	    	media=getMediaImages(v.mediaImg,e,v.author.id,v.target.name);
	      	$("#resultImg"+e).html(media);
	    }else if(mediaType=="file"){
	    	media=getMediaFiles(v.mediaFile,e);
	      	$("#resultFile"+e).html(media);
	    }

	}
}
var newsCustom = {}
var tagsFilterListHTML = "";
var scopesFilterListHTML = "";
/*
* function loadStream() loads news for timeline: 5 news are download foreach call
* @param string contextParentType indicates type of wall news
* @param string contextParentId indicates the precise parent id 
* @param strotime dateLimite indicates the date to load news
*/
var loading = 	"<div class='loader shadow2 bold letter-blue text-center margin-bottom-50'>"+
					"<span style=''>"+
						"<i class='fa fa-spin fa-circle-o-notch'></i> "+
						"<span>"+trad.currentlyloading+" ...</span>" + 
				"</div>";

var colNotifOpen = true;


function smoothScroll(scroolTo){
	$(".my-main-container").scrollTo(scroolTo,500,{over:-0.6});
}

function modifyNews(idNews,typeNews){
	var idNewsUpdate=idNews;
	var typeNewsUpdate=typeNews;
	var commentContent = ($('.newsContent[data-pk="'+idNews+'"] .allText').length) ? $('.newsContent[data-pk="'+idNews+'"] .allText').html() :  $('.newsContent[data-pk="'+idNews+'"] .timeline_text').html();
	var commentTitle = $('.newsTitle[data-pk="'+idNews+'"] .timeline_title').html();
	var message = "<div id='form-news-update'>";
	var scopeTarget=updateNews[idNews].scope.type;
	var newsScope={};
	var newsTargetType=updateNews[idNews].target.type;
	var scopeLabel=contextScopeNews[newsTargetType][scopeTarget].label;
	var scopeIcon=contextScopeNews[newsTargetType][scopeTarget].icon;
	if(notEmpty(commentTitle))
		message += "<input type='text' id='textarea-edit-title"+idNews+"' class='form-control margin-bottom-5' style='text-align:left;' placeholder='Titre du message' value='"+commentTitle+"'>";
	 	
	message += "<div id='container-txtarea-news-"+idNews+"' class='updateMention'>";
		message += 	"<textarea id='textarea-edit-news"+idNews+"' class='form-control newsContentEdit newsTextUpdate get_url_input' placeholder='modifier votre message'>"+commentContent+"</textarea>"+
				   	"<div id='results' class='bg-white results col-sm-12 col-xs-12 margin-top-20'>";
				   	if(typeof updateNews[idNews]["media"] != "undefined"){
				   		if(updateNews[idNews]["media"]["type"]=="url_content")
				   			message += processUrl.getMediaCommonHtml(updateNews[idNews]["media"],"save");
				   		else if(updateNews[idNews]["media"]["type"]=="gallery_files"){
				   			message += getMediaFiles(updateNews[idNews]["media"],idNews, "update")+
				   			"<input type='hidden' class='type' value='gallery_files'>";
				   		}else if (updateNews[idNews]["media"]["type"]=="gallery_images"){
				   			message += getMediaImages(updateNews[idNews]["media"], idNews,null,null, "update")+
				   			"<input type='hidden' class='type' value='gallery_images'>";
				   		}else{
				   			message +='<a href="javascript:;" class="removeMediaUrl"><i class="fa fa-times"></i></a>'+
			                directory.showResultsDirectoryHtml(new Array(updateNews[idNews]["media"]["object"]))+
                			"<input type='hidden' class='type' value='activityStream'>"+
							"<input type='hidden' class='objectId' value='"+updateNews[idNews]["media"]["object"]["_id"]["$id"]+"'>"+
							"<input type='hidden' class='objectType' value='"+updateNews[idNews]["media"]["object"]["type"]+"'>";
				   		}
				   	}
		message +=	"</div>"+
					"<div id='resultsImg' class='bg-white results col-sm-12 col-xs-12 margin-top-20'>";
				   		if(typeof updateNews[idNews]["mediaImg"] != "undefined")
				   			message += getMediaImages(updateNews[idNews]["mediaImg"],idNews,  null, null, "update");
		message +=	"</div>"+
					"<div id='resultsFile' class='bg-white results col-sm-12 col-xs-12 margin-top-20'>";
				   		if(typeof updateNews[idNews]["mediaFile"] != "undefined")
				   			message += getMediaFiles(updateNews[idNews]["mediaFile"],idNews, "update");
		message +=	"</div>"+
					'<div class="form-group tagstags col-md-12 col-sm-12 col-xs-12 margin-top-20 no-padding">'+
          				'<input id="tags" type="" data-type="select2" name="tags" placeholder="#Tags" value="" style="width:100%;">'+       
      				"</div>"+
      				'<div class="dropdown no-padding col-md-12 col-sm-12 col-xs-12 margin-bottom-20">'+
          				'<a data-toggle="dropdown" class="btn btn-default col-md-12 col-sm-12 col-xs-12" id="btn-toogle-dropdown-scope-update" href="javascript:;">'+
          					'<i class="fa fa-'+scopeIcon+'"></i> '+scopeLabel+' <i class="fa fa-caret-down" style="font-size:inherit;"></i>'+
          				'</a>'+
          				'<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
          					$.each(contextScopeNews[newsTargetType], function(e, v){
          						if(e != "init"){
          	message+=				'<li>'+
              							'<a href="javascript:;" id="scope-my-network" class="scopeShare" data-value="'+e+'">'+
              								'<h4 class="list-group-item-heading"><i class="fa fa-'+v.icon+'"></i> '+v.label+'</h4>'+
               								'<p class="list-group-item-text small">'+v.explain+'</p>'+
              							'</a>'+
            						'</li>';
          						}
          					});
			message+=   '</ul>'+
			            '<input type="hidden" name="scope" id="scope" value="'+scopeTarget+'"/>'+
	        		'</div>'+
	        		'<div id="scopeListContainerFormUpdate" class="form-group col-md-12 col-sm-12 col-xs-12 no-padding margin-bottom-10"></div>'+
					'<div id="error-update" class="form-group col-xs-12 no-padding margin-bottom-10"></div>'+
				"</div>";
	
	message+="</div>";

	var boxComment = bootbox.dialog({
	  message: message,
	  title: trad.updatethepost,
	  backdrop:true,
	  buttons: {
	  	annuler: {
	      label: trad.cancel,
	      className: "btn-default",
	      callback: function() {
	        mylog.log("Annuler");
	      }
	    },
	    enregistrer: {
	      label: trad.save,
	      className: "btn-success",
	      callback: function() {
	      	$("#btn-submit-form").prop('disabled', true);
			$("#btn-submit-form i").removeClass("fa-arrow-circle-right").addClass("fa-circle-o-notch fa-spin");
			$("#error-update").empty();
			error=[];
			if($("#form-news-update .noGoSaveNews").length)
				error.push("waitendofloading");
			if($("#form-news-update #results").html() == ""
			 	&& $("#form-news-update .get_url_input").val()== ""
			 	&& $("#form-news-update #resultsImg").html()== ""
			 	&& $("#form-news-update #resultsFile").html()== "")
				error.push("writesomethingplease");
			//if($("#form-news-update input[name='scope']").val()=="public" && Object.keys(newsScopes).length==0)
			//	error.push("addplacesplease");
			if(error.length > 0){
				$.each(error, function(e, v){
					$("#error-update").before("<span class='help-block "+v+" italic'>* "+trad[v]+"</span>");
				});
				$("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
				$("#btn-submit-form").prop('disabled', false);		
				return false;
			}else{
				heightCurrent=$("#"+typeNewsUpdate+idNewsUpdate).find(".timeline-panel").height();
	      		$("#"+typeNewsUpdate+idNewsUpdate).find(".timeline-panel").append("<div class='updateLoading' style='line-height:"+heightCurrent+"px'><i class='fa fa-spin fa-spinner'></i> En cours de modification</div>");
	    		newNews = getNewsObject("#form-news-update", "update");// new Object;
	    		newNews.idNews = idNews;	
				newNews=mentionsInit.beforeSave(newNews, '.newsTextUpdate');	
				ajaxPost(
			        null,
			        baseUrl+"/news/co/update?tpl=co2",
			        newNews,
			        function(data){ 
			        	if(data)
			    		{
			    			$("#"+typeNewsUpdate+idNewsUpdate).replaceWith(data);
			    			newsObj.bindEvent();
			    			toastr.success("Votre publication a bien été modifié");
							return true;
			    		}
			    		else 
			    		{
							toastr.error(data.msg);
			    		}
			    		$("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
						$("#btn-submit-form").prop('disabled', false);
						return false;
			        },
			        function(data){
			        	toastr.error("Something went wrong, contact your admin"); 
					   $("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
					   $("#btn-submit-form").prop('disabled', false);
			        }
		        );
			}
	      }
	    },
	  }
	});

	boxComment.on("shown.bs.modal", function() {
	  $.unblockUI();
	  bindEventTextAreaNews('#textarea-edit-news'+idNews, idNews, updateNews[idNews]);
	});
}
function updateNews(idNews, newText, type){
	var classe1=""; var classe2="";
	if(type == "newsContent") { classe1="text"; classe2=".newsContent"; }else{ classe1="title";classe2=".newsTitle"; }
	updateField("News",idNews,type,newText,false);
	$(classe2+'[data-pk="'+idNews+'"] .timeline_'+classe1).html(newText);
}

function bindEventTextAreaNews(idTextArea, idNews,data){
	processUrl.getMediaFromUrlContent(idTextArea,"#results",1);
	if($("#form-news-update #results").html() !="") $("#form-news-update #results").show();
	if($("#form-news-update #resultsImg").html() !="") $("#form-news-update #resultsImg").show();
	if($("#form-news-update #resultsFile").html() !="") $("#form-news-update #resultsFile").show();
	
	mentionsInit.get(idTextArea);
	$(".removeMediaUrl").click(function(){
        $trigger=$(this).parents().eq(1).find(idTextArea);
	    $(idTextArea).parents().eq(1).find("#results").empty().hide();
	});
	$(".deleteDocImg").click(function(){
		$(this).parent().remove();
		if($(".deleteDocImg").length == 0)
			$("#form-news-update  #resultsImg").empty().hide();
	});

	$(".deleteDocFile").click(function(){
		$(this).parent().remove();
		if($(".deleteDocFile").length == 0)
			$("#form-news-update  #resultsFile").empty().hide();
	});
	autosize($(idTextArea));
	textNews=data.text;
	$(idTextArea).val(textNews);
	if(typeof data.mentions != "undefined" && data.mentions.length != 0)
		$(idTextArea).mentionsInput("update", data.mentions);
	$(idTextArea).on('keyup ', function(e){
		var heightTxtArea = $(idTextArea).css("height");
    	$("#container-txtarea-news-"+idNews).css('height', heightTxtArea);
	});
	$(idTextArea).bind ("input propertychange", function(e){
		var heightTxtArea = $(idTextArea).css("height");
    	$("#container-txtarea-news-"+idNews).css('height', heightTxtArea);
	});
      //$('#form-news-update #tags').select2({tags:tagsNews});
      
      $('#form-news-update #tags').select2({
          minimumInputLength: 3,
          tags: true,
          multiple: true,
          createSearchChoice: function (term, data) {
              if (!data.length)
                  return {
                      id: term,
                      text: term
                  };
          },
          ajax: {
              url: baseUrl+'/api/tags/search',
              dataType: 'json',
              type: "GET",
              quietMillis: 50,
              data: function (term) {
                  return {
                      q: term
                  };
              },
              results: function (data) {
              	mylog.log("bindEventTextAreaNews 4 tags/search ", data);
                  return {
                      results: $.map(data, function (item) {
                          return {
                              text: item.tag,
                              id: item.tag
                          }
                      })
                  };
              }
          }
      });
     if(typeof data.tags != "undefined"){
     	$("#form-news-update #tags").select2('data', $.map(data.tags, function (item) {
	  	    return {
	  	        text: item,
	  	        id: item
	  	    }
	  	}));
     }
  	
  	if( typeof data.scope != "undefined" && 
  		data.scope.type == "public" && 
  		typeof data.scope.localities != "undefined"){
  		convertScopeForUpdate(data.scope.localities);
		getScopeNewsHtml("#scopeListContainerFormUpdate");
		$("#scopeListContainerFormUpdate").show(700);
	}

  	$("#form-news-update .scopeShare").click(function() {
		replaceText=$(this).find("h4").html();
		$("#btn-toogle-dropdown-scope-update").html(replaceText+' <i class="fa fa-caret-down" style="font-size:inherit;"></i>');
		scopeChange=$(this).data("value");
		$("#form-news-update input[name='scope']").val(scopeChange);
		if(scopeChange == "public"){
			if($("#scopeListContainerFormUpdate").html()=="") getScopeNewsHtml("#scopeListContainerFormUpdate");
			$("#scopeListContainerFormUpdate").show(700);
	  	}else
	  		$("#scopeListContainerFormUpdate").hide(700);
	});
	mylog.log("bindEventTextAreaNews 6");
}

function convertScopeForUpdate(loc){
	newsScopes={};
	$.each(loc, function(e, v){
		if(typeof v.postalCode != "undefined"){
			newsScopes[v.postalCode+"cp"]={
				"type":"cp",
				"name":v.postalCode
			}
		}else if(typeof v.name != "undefined"){
			newsScopes[v.parentId+v.parentType]={
				"type":v.parentType,
				"id":v.parentId,
				"name":v.name
			};
			if(typeof v.cp != "undefined")
				newsScopes[v.parentId+v.parentType]["cp"]=v.cp;
		}
	});
}

function deleteNews(id, type, $this){
	bootbox.confirm(trad["suretodeletenews"], 
		function(result) {
			if (result) {
				if ($(".deleteImageIdName"+id).length){
					$(".deleteImageIdName"+id).each(function(){
						deleteInfo=$(this).val().split("|");
						deleteImage(deleteInfo[0],deleteInfo[1],true);
						
					});
				}
				if ($("#deleteImageCommunevent"+id).length){
						imageId=$("#deleteImageCommunevent"+id).val();
						deleteImage(imageId,"",true,true);
				}
				ajaxPost(
			        null,
			        baseUrl+"/news/co/delete/id/"+id,
			        {"isLive": isLive},
			        function(data){ 
			        	if (data) {  
			        		if(typeof data.result != "undefined"){    
			        			if(typeof data.share != "undefined")
			        				toastr.success("Votre partage a été supprimé avec succès!!");
			        			else         
									toastr.success(trad["successdeletenews"] + "!!");
								$("#"+type+id).fadeOut();
							} else {
								toastr.success("Votre partage a été supprimé avec succès!!");
								$("#"+type+id).replaceWith(data);
							}	
						} else {
				            toastr.error(trad["somethingwrong"] + " " + trad["tryagain"]);
				        }
			        }
		        );
			}
		}
	)
}

 function insertNews(newsObj)
 {
 	var date = new Date( parseInt(newsObj.created.sec)*1000 );
 	if(newsObj.date.sec && newsObj.date.sec != newsObj.created.sec) {
 		date = new Date( parseInt(newsObj.date.sec)*1000 );
 	}
 	var newsTLLine = buildLineHTML(newsObj,idSession,true);
 	$(".emptyNews").remove();
 	$("#newFeedForm").parent().after(newsTLLine).fadeIn();
 	$("#newFeedForm").parent().next().css("margin-top","20px");
 	manageModeContext(newsObj._id.$id);
 	$("#form-news #get_url").val("");
 	$('textarea.mention').mentionsInput('reset');
 	$("#form-news #results").html("").hide();
 	$("#form-news #resultsImg").html("").hide();
 	$("#form-news #resultsFile").html("").hide();
 	
 	$("#form-news #tags").select2('val', "");
 	showFormBlock(false);
 	$('.tooltips').tooltip();
 	newsObj.bindEvent();
 }


function applyTagFilter(str)
{
	$(".newsFeed").fadeOut();
	if(!str){
		if($(".btn-tag.active").length){
			str = "";
			sep = "";
			$.each( $(".btn-tag.active") , function() { 
				str += sep+"."+$(this).data("id");
				sep = ",";
			});
		} else
			str = ".newsFeed";
	} 
	mylog.log("applyTagFilter",str);
	$(str).fadeIn();
	return $(".newsFeed").length;
}

function applyScopeFilter(str)
{
	$(".newsFeed").fadeOut();
	if(!str){
		if($(".btn-context-scope.active").length){
			str = "";
			sep = "";
			$.each( $(".btn-context-scope.active") , function() { 
				str += sep+"."+$(this).data("val");
				sep = ",";
			});
		} else
			str = ".newsFeed";
	} 
	mylog.log("applyScopeFilter",str);
	$(str).fadeIn();
	return $(".newsFeed").length;
}

function toggleFilters(what){
 	if( !$(what).is(":visible") )
 		$('.optionFilter').hide();
 	$(what).slideToggle();
 }
 function getScopeNewsHtml(target){
 	scopeHtml ='<div id="scopes-news-form" class="no-padding">'+
 			'<div id="news-scope-search" class="col-md-12 col-sm-12 col-xs-12 no-padding">'+
	 			'<label class="margin-left-5"><i class="fa fa-angle-down"></i> '+trad.addplacestyournews+'</label><br>'+
	 			'<div class="bg-white padding-10">'+
	            '<div id="input-sec-search" class="hidden-xs col-xs-12 col-md-4 col-sm-4 col-lg-4">'+
	                '<div class="input-group shadow-input-header">'+
	                      '<span class="input-group-addon bg-white addon-form-news"><i class="fa fa-search fa-fw" aria-hidden="true"></i></span>'+
	                      '<input type="text" class="form-control input-global-search" id="searchOnCityNews" autocomplete="off" placeholder="'+trad.searchcity+' ...">'+
	                '</div>'+
	                '<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" style="max-height: 70%; display: none;"><div class="text-center" id="footerDropdownGS"><label class="text-dark"><i class="fa fa-ban"></i> Aucun résultat</label><br></div>'+
	                '</div>'+
	            '</div>'+
            	'<a href="javascript:;" id="multiscopes-news-btn" class="scopes-btn-news margin-left-20" data-type="multiscopes"><i class="fa fa-star"></i> '+trad.facoritesplaces+'</a>'+
            	'<a href="javascript:;" id="communexion-news-btn" class="scopes-btn-news  margin-left-20" data-type="communexion"><i class="fa fa-home"></i> <span class="communexion-btn-label"></span></a>'+
            	'</div>'+
            '</div>'+
            '<div id="news-scopes-container" class="scopes-container col-md-12 col-sm-12 col-xs-12">'+
            '<hr class="submit">'+
            '</div>'+
            '<div class="col-md-12 col-sm-12 col-xs-12 margin-top-10 no-padding">'+
            	'<label class="margin-left-5"><i class="fa fa-angle-down"></i> '+trad.selectedzones+'</label><br>'+
            '</div>'+
            '<div id="content-added-scopes-container" class="col-md-12 col-sm-12 col-xs-12">';
            if(Object.keys(newsScopes).length > 0){
            	$.each(newsScopes, function(key,value){
            		btnScopeAction="<span class='manageMultiscopes tooltips margin-right-5 margin-left-10' "+
						"data-add='false' data-scope-value='"+value.id+"' "+
						'data-scope-key="'+key+'" '+
						"data-toggle='tooltip' data-placement='top' "+
						"data-original-title='Remove'>"+
							"<i class='fa fa-times-circle'></i>"+
						"</span>";
			    	scopeHtml += "<div class='scope-order text-red' data-level='"+value.level+"'>"+
			    				btnScopeAction+
			    				"<span data-toggle='dropdown' data-target='dropdown-multi-scope' "+
									"class='item-scope-checker item-scope-input' "+
									'data-add="false" '+
									'data-scope-key="'+key+'" '+
									'data-scope-value="'+value.id+'" '+
									'data-scope-name="'+value.name+'" '+
									'data-scope-type="'+value.type+'" '+
									'data-scope-level="'+value.type+'" ';
									if(notNull(value.level))
										scopeHtml += 'data-level="'+value.level+'"';
									scopeHtml += '>' + 
									value.name + 
								"</span>"+
							"</div>";
			    });
            }

    scopeHtml+='</div>';
        '</div>';
	domForm=(notNull(target))?target:"#scopeListContainerForm";
	$(domForm).html(scopeHtml);
	bindSearchOnNews();
	bindScopesNewsEvent();
	$("#multiscopes-news-btn").trigger("click");
	getCommunexionLabel();
}
function bindSearchOnNews(){
    $("#searchOnCityNews").off().on("keyup", function(e){
    	e.preventDefault();
        if(e.keyCode == 13){
            searchTypeGS = ["cities"];
            startGlobalSearch(0, 30, "#scopes-news-form");
         }
    });
}
function bindScopesNewsEvent(news){
	mylog.log("bindScopesNewsEvent", news);
	$(".manageMultiscopes, #news-scopes-container .item-scope-checker, #content-added-scopes-container .item-scope-checker").off().on("click", function(){
		mylog.log("manageMultiscopes");
		addScope=$(this).data("add");
		scopeValue=$(this).data("scope-value");
		key=$(this).data("scope-key");
		mylog.log("manageMultiscopes key scopeValue addScope", key, scopeValue, addScope);
		if(addScope){
			newScope=myScopes[myScopes.typeNews][key];
			newScope.active=true;
			newsScopes[key] = newScope;
			//$(this).attr("data-add",false).attr("data-original-title","Remove");
			$("span[data-scope-key='"+key+"']").attr("data-add",false).attr("data-original-title","Remove");
			//$(this).find("i").removeClass("fa-plus-circle").addClass("fa-times-circle");
			$(this).parent().find(".manageMultiscopes i").removeClass("fa-plus-circle").addClass("fa-times-circle");
			pushHtml=$(this).parent().get();
			$(this).parent().remove();
			mylog.log("manageMultiscopes pushHtml", pushHtml);
			$("#content-added-scopes-container").append(pushHtml);
			$(".addplacesplease").remove();
			bindScopesNewsEvent();
		}else{
			mylog.log("manageMultiscopes remove", key, newsScopes);
			$("span[data-scope-key='"+key+"']").attr("data-add",true).attr("data-original-title","Add scope");
			//$(this).find("i").removeClass("fa-plus-circle").addClass("fa-times-circle");
			$(this).parent().find(".manageMultiscopes i").removeClass("fa-times-circle").addClass("fa-plus-circle");
			pushHtml=$(this).parent().get();
			delete newsScopes[key];
			$(this).parent().remove();
			$("#news-scopes-container").append(pushHtml);
			bindScopesNewsEvent();
		}
	});
	
	$("#multiscopes-news-btn, #communexion-news-btn").off().on("click", function(){
		mylog.log("#multiscopes-news-btn, #communexion-news-btn");
		$(".scopes-btn-news").removeClass("active");
		$(this).addClass("active");
		myScopes.typeNews=$(this).data("type");
		$("#scopes-news-form .scopes-container").html(constructScopesHtml(true));
		if(myScopes.typeNews=="communexion")
				$("#scopes-news-form .scopes-container .scope-order").sort(sortSpan) // sort elements
                  .appendTo("#scopes-news-form .scopes-container");
        bindScopesNewsEvent();
	});
}
/*
* Save news and url generate
*
*/
function showFormBlock(bool){
	if(bool){
		$(".form-create-news-container #text").show("fast");
		$(".form-create-news-container .tagstags").show("fast");
		$(".form-create-news-container .datedate").show("fast");
		$(".form-create-news-container .form-actions").show("fast");
		$(".form-create-news-container .publiccheckbox").show("fast");
		$(".form-create-news-container .tools_bar").show("fast");
		$(".form-create-news-container .scopescope").show("fast");
		/////multiTagScopeLbl("send");
		if($(".form-create-news-container input[name='scope']").val()=="public")
			scopeHtml=getScopeNewsHtml();
		
		$('.extract_url').show();
		$(".form-create-news-container #falseInput").hide();
		$('#get_url').focus();
		
		$("#toogle_filters").hide();	
		$(".form-create-news-container #btn-slidup-scopetags").hide("fast");
		//if(typeof slidupScopetagsMin != "undefined") slidupScopetagsMin(false);
		
	}else{
		$(".form-create-news-container #text").hide();
		$(".form-create-news-container .tagstags").hide();
		$(".form-create-news-container .datedate").hide();
		$(".form-create-news-container .form-actions").hide();
		$(".form-create-news-container .publiccheckbox").hide();
		$(".form-create-news-container .tools_bar").hide();
		$(".form-create-news-container .scopescope").hide();
		$("#scopeListContainerForm").html("");
		$("#container-scope-filter").show();
		$(".item-globalscope-checker").prop('disabled', false);
		//SCOPE DIV//
		$('.extract_url').hide();
		$(".form-create-news-container #falseInput").show();
		$("#toogle_filters").show();	
		$(".form-create-news-container #btn-slidup-scopetags").show("fast");
		
	}
}

function saveNews(){
	var formNews = $('#form-news');
	var errorHandler2 = $('.errorHandler', formNews);
	var successHandler2 = $('.successHandler', formNews);

	var validation = {
		submitHandler : function(form) {
			$('#modalLogin').modal("show");
		}
	};
	if(userId != null && userId != ""){
		$("#btn-submit-form").prop('disabled', true);
		$("#btn-submit-form i").removeClass("fa-arrow-circle-right").addClass("fa-circle-o-notch fa-spin");
		$(".form-create-news-container .form-actions .help-block").remove();
		error=[];
		if($(".noGoSaveNews").length)
			error.push("waitendofloading");
		if($("#form-news #results").html() == ""
			&& $("#form-news #resultsImg").html() == ""
			&& $("#form-news #resultsFile").html() == "" 
			&& $("#form-news #get_url").val()== "")
			error.push("writesomethingplease");
		//if($("input[name='scope']").val()=="public" && Object.keys(newsScopes).length==0)
		//	error.push("addplacesplease");
		if(error.length > 0){
			$.each(error, function(e, v){
				$("#btn-submit-form").before("<span class='help-block "+v+" italic'>* "+trad[v]+"</span>");
			});
			$("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
			$("#btn-submit-form").prop('disabled', false);		
		}else{
			newNews=getNewsObject("#form-news");
			newNews=mentionsInit.beforeSave(newNews, 'textarea.mention');
			ajaxPost(
		        null,
		        baseUrl+"/news/co/save",
		        newNews,
		        function(data){ 
		        	if(data)
		    		{
		    			$("#form-news #get_url").val("");
		    			mentionsInit.reset('textarea.mention');
						newsScopes={};
						$("#form-news #results").html("").hide();
						$("#form-news #resultsImg").html("").hide();
						$("#form-news #resultsFile").html("").hide();
						
						$("#form-news #tags").select2('val', "");
						showFormBlock(false);
		    			$("#news-list").prepend(data);
		    			newsObj.bindEvent();
			    		toastr.success(trad["successsavenews"]);
			    		afterSaveNews(data);
			    	}
			    	else 
			    		toastr.error(data.msg);
						
		    		$("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
					$("#btn-submit-form").prop('disabled', false);
					return false;
		        },
		        function(data){
		        	 toastr.error("Something went wrong, contact your admin"); 
				   $("#btn-submit-form i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-arrow-circle-right");
				   $("#btn-submit-form").prop('disabled', false);
		        }
	        );
		}
	}
}

function afterSaveNews(data){
	mylog.log("save news callback",data);
	if(typeof data=="string"){
	    extractedId=data.substring(data.indexOf('id="new') + 8, data.indexOf('id="new') + 32);
	    data={
	    	id : extractedId
	    };
	    //alert(extractedId);
	}	
	
	if (costum && costum.slug && costum[costum.slug] && costum[costum.slug].news && costum[costum.slug].news.afterSave && typeof costum[costum.slug].news.afterSave === "function")
		costum[costum.slug].news.afterSave(data);
}

function getNewsObject(formId, actionType){
	newNews = {
		text : $(formId+" .get_url_input").val(),
		scope : $(formId+" input[name='scope']").val(),
		markdownActive: $("#fcn-active-markdown").is(':checked')
	};
	
	var actionType=(notNull(actionType))? actionType : "save";
	
	if($(formId+" #results").html() != ""){
		newNews.media=new Object;	
		newNews.media.type=$(formId+" #results .type").val();
		if(newNews.media.type=="url_content"){
			if($(formId+" #results .name").length)
				newNews.media.name=$(formId+" #results .name").val();
			if($(formId+" #results .description").length)
				newNews.media.description=$(formId+" #results .description").val();
			newNews.media.content=new Object;
			newNews.media.content.type=$(formId+" #results .media_type").val(),
			newNews.media.content.url=$(formId+" #results .url").val(),
			newNews.media.content.image=$(formId+" #results .img_link").val();
			if($(formId+" #results .size_img").length)
				newNews.media.content.imageSize=$(formId+" #results .size_img").val();
			if($(formId+" #results .video_link_value").length)
				newNews.media.content.videoLink=$(formId+" #results .video_link_value").val();
		}else if(newNews.media.type=="activityStream"){
			newNews.media.object={
				"id":$(formId+" #results .objectId").val(),
				"type":$(formId+" #results .objectType").val()
			}
		}
		/*else if(newNews.media.type=="gallery_images"){
			newNews.media.type=$(formId+" #results .type").val(),
			newNews.media.countImages=$(formId+" #results .docsId").length;
			if(newNews.media.countImages>0){
				newNews.media.images=[];
				$(formId+" #results .docsId").each(function(){
					newNews.media.images.push($(this).val());	
				});	
			}else if(actionType=="update")
				newNews.media="unset";
		}*/
		/*else if($(formId+" #results .type").val()=="gallery_files"){
			newNews.media.countFiles=$(formId+" #results .docsId").length;
			if(newNews.media.countFiles>0){
				newNews.media.files=[];
				$(".docsId").each(function(){
					newNews.media.files.push($(this).val());	
				});
			}else if(actionType=="update")
				newNews.media="unset";		
		}*/
	}else if(actionType=="update")
		newNews.media="unset";
	
	if($(formId+" #resultsImg").html() != ""){
		newNews.mediaImg=new Object;	
		//newNews.mediaImg.type=$(formId+" #resultsImg .type").val(),
		newNews.mediaImg.countImages=$(formId+" #resultsImg .docsId").length;
		if(newNews.mediaImg.countImages>0){
			newNews.mediaImg.images=[];
			$(formId+" #resultsImg .docsId").each(function(){
				newNews.mediaImg.images.push($(this).val());	
			});	
		}else if(actionType=="update")
			newNews.mediaImg="unset";
	}else if(actionType=="update")
		newNews.mediaImg="unset";
   
	
	if($(formId+" #resultsFile").html() != ""){
		newNews.mediaFile=new Object;	
	//	newNews.mediaFile.type=$(formId+" #resultsFile .type").val();
		newNews.mediaFile.countFiles=$(formId+" #resultsFile .docsId").length;
		if(newNews.mediaFile.countFiles>0){
			newNews.mediaFile.files=[];
			$(formId+" #resultsFile .docsId").each(function(){
				newNews.mediaFile.files.push($(this).val());	
			});
		}else if(actionType=="update")
			newNews.mediaFile="unset";		
	}else if(actionType=="update")
		newNews.mediaFile="unset";
	
	if ($(formId+" #tags").val() != "")
		newNews.tags = $(formId+" #tags").val().split(",");

	if(actionType=="save"){		
		newNews.parentId = $(formId+" #parentId").val(),
		newNews.parentType = $(formId+" #parentType").val(),
		newNews.type = $(formId+" input[name='type']").val();
		if( typeof costum != "undefined" && notNull(costum) && typeof costum.request != "undefined" && typeof costum.request.sourceKey != "undefined"){
			newNews.source = { 
				insertOrign : "costum",
				keys : [ 
					costum.request.sourceKey[0]
				],
				key : costum.request.sourceKey[0]
			};
		}
	}
	
	if(newNews.scope=="public" && Object.keys(newsScopes).length > 0)
		newNews.localities = newsScopes;

	if($(formId+' #authorIsTarget').length && $(formId+' #authorIsTarget').val()==1)
		newNews.targetIsAuthor = true;
	
	return newNews;
}

function showAllNews(){ //
	$(".newsFeed").show();
	$('.optionFilter').hide();
}

/* Notuse => function added to object 'newsObj'
function initFormImages(){
	$("#photoAddNews").on('submit',(function(e) {
		e.preventDefault();
		if(contextParentType=="city" || contextParentType=="pixels"){
			contextParentType = "citoyens";
			contextParentId = idSession;
		}
		extraParamsAjax={
		        contentType: false,
			cache: false, 
			processData: false
	    };
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/document/uploadSave/dir/communecter/folder/"+contextParentType+"/ownerId/"+contextParentId+"/input/newsImage/docType/image/contentKey/slider",
	        new FormData(this),
	        function(data){ 
	        	if(debug)mylog.log(data);
	  		    if(data.result){
				    toastr.success(data.msg);
				    //setTimeout(function(){
				    $(".imagesNews").last().val(data.id.$id);
				    $(".imagesNews").last().attr("name","");
				    $(".newImageAlbum").last().find("img").removeClass("grayscale");
				    $(".newImageAlbum").last().find("i").remove();
				    $(".newImageAlbum").last().append("<a href='javascript:;' onclick='deleteImage(\""+data.id.$id+"\",\""+data.name+"\")'><i class='fa fa-times fa-x padding-5 text-white removeImage' id='deleteImg"+data.id.$id+"'></i></a>");
				    //},200);
		
				} else{
					toastr.error(data.msg);
					if($("#results img").length>1)
				  		$(".newImageAlbum").last().remove();
				  	else{
				  		$("#results").empty();
				  		$("#results").hide();
				  	}
				}
				$("#addImage").off();
				$(".form-create-news-container .form-actions .waitendofloading").remove();
	        }
        );
		$.oooooooooooooooooooooajax({
			url : baseUrl+"/"+moduleId+"/document/uploadSave/dir/communecter/folder/"+contextParentType+"/ownerId/"+contextParentId+"/input/newsImage/docType/image/contentKey/slider",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false, 
			processData: false,
			dataType: "json",
			success: function(data){
				if(debug)mylog.log(data);
	  		    if(data.result){
				    toastr.success(data.msg);
				    //setTimeout(function(){
				    $(".imagesNews").last().val(data.id.$id);
				    $(".imagesNews").last().attr("name","");
				    $(".newImageAlbum").last().find("img").removeClass("grayscale");
				    $(".newImageAlbum").last().find("i").remove();
				    $(".newImageAlbum").last().append("<a href='javascript:;' onclick='deleteImage(\""+data.id.$id+"\",\""+data.name+"\")'><i class='fa fa-times fa-x padding-5 text-white removeImage' id='deleteImg"+data.id.$id+"'></i></a>");
				    //},200);
		
				} else{
					toastr.error(data.msg);
					if($("#results img").length>1)
				  		$(".newImageAlbum").last().remove();
				  	else{
				  		$("#results").empty();
				  		$("#results").hide();
				  	}
				}
				$("#addImage").off();
				$(".form-create-news-container .form-actions .waitendofloading").remove();
			}
		});
	}));
}*/

function addMoreSpace(){
	bootbox.dialog({
	message: "You have attempt the limit of 20Mo of images for this "+contextParentType+"<br/>Please choose one of those  two solutions beyond:<br/>Delete images in the <a href='javascript:;' onclick='bootbox.hideAll();urlCtrl.loadByHash(\"#gallery.index.id."+contextParentId+".type."+contextParentType+"\")'>photo gallery</a> <br/><br/>OR<br/><br/> Subscribe 12€ to the NGO Open Atlas which takes in charge communecter.org on <a href='https://www.helloasso.com/associations/open-atlas' target='_blank'>helloAsso</a> for 20Mo more. <br/><br/>Effectively, stocking images represents a cost for us and donate to the NGO will demonstrate your contribution the project and to the common we built together",
  title: "Limit of <color class='red'>20 Mo</color> overhead"
  });
}

function showMyImage(fileInput) {
	if($(".noGoSaveNews").length){
		toastr.info(trad.waitendofloading);
	}
	else if (fileInput.files[0].size > 15000000){
		toastr.info("Please reduce your image before to 15Mo");
	}
	else {
		countImg=$("#resultsImg img").length;
		idImg=countImg+1;
		htmlImg="";
		var files = fileInput.files;
		if(countImg==0){
			htmlImg = "<input type='hidden' class='type' value='gallery_images'/>";
			htmlImg += "<input type='hidden' class='count_images' value='"+idImg+"'/>";
			htmlImg += "<input type='hidden' class='algoNbImg' value='"+idImg+"'/>";
			nbId=idImg;
			$("#resultsImg").show();
		}
		else{
			nbId=$(".algoNbImg").val();
			nbId++;
			$(".count_images").val(idImg);
			$(".algoNbImg").val(nbId);
		}
		htmlImg+="<div class='newImageAlbum'><i class='fa fa-spin fa-circle-o-notch fa-3x text-green spinner-add-image noGoSaveNews'></i><img src='' id='thumbail"+nbId+"' class='grayscale' style='width:75px; height:75px;'/>"+
		       	"<input type='hidden' class='imagesNews docsId' name='goSaveNews' value=''/></div>";
		$("#resultsImg").append(htmlImg);
	    for (var i = 0; i < files.length; i++) {           
	        var file = files[i];
	        var imageType = /image.*/;     
	        if (!file.type.match(imageType)) {
	            continue;
	        }           
	        var img=document.getElementById("thumbail"+nbId);            
	        img.file = file;    
	        var reader = new FileReader();
	        reader.onload = (function(aImg) { 
	            return function(e) { 
	                aImg.src = e.target.result; 
	            }; 
	        })(img);
	        reader.readAsDataURL(file);
	    }  
		$("#photoAddNews").submit();	  
	}
}

function getMediaImages(o,newsId,authorId,targetName, actionType){
	countImages=o.images.length;
	html="";
			
	if(typeof actionType != "undefined" && actionType=="update"){
		for(var i in o.images){
	
			html+="<div class='updateImageNews'><img src='"+o.images[i].imageThumbPath+"' style='width:75px; height:75px;'/>"+
		       	"<a href='javascript:;' class='btn-red text-white deleteDocImg deleteDoc'><i class='fa fa-times text-dark'></i></a>"+
					"<input type='hidden' class='docsId' value='"+o.images[i]._id.$id+"'></div>";
		}
		return html;
	}
	else if(typeof actionType != "undefined" && actionType=="directory"){
		//path=baseUrl+"/"+uploadUrl+"communecter/"+o.images[0].folder+"/"+o.images[0].name;
		html+="<img src='"+o.images[0].imagePath+"' class='img-responsive'>";
	}else{
		if(countImages==1){
			path=o.images[0].imagePath;
			html+="<div class='col-md-12 no-padding margin-top-10 margin-bottom-10'><a class='thumb-info' href='"+path+"' data-title='album de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style=''></a></div>";
		}
		else if(countImages==2){
			for(var i in o.images){
				path=o.images[i].imagePath;
				html+="<div class='col-md-6 padding-5'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='max-height:200px;'></a></div>";
			}
		}
		else if(countImages==3){
			col0="6";
			height0="400";
			absoluteImg="position:absolute;";
			if (typeof liveScopeType != "undefined" && liveScopeType == "global"){
				col0="12";
				height0="260";
				absoluteImg="";
			}
			for(var i in o.images){
				path=o.images[i].imagePath;
				if(i==0){
				html+="<div class='col-md-"+col0+" padding-5' style='position:relative;height:"+height0+"px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+"min-height:100%;min-width:100%;'></a></div>";
				}else{
				html+="<div class='col-md-6 padding-5' style='position:relative; height:200px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+"min-height:100%;min-width:100%;'></a></div>";	
				}
			}
		}
		else if(countImages==4){
			absoluteImg="position:absolute;";
			if (typeof liveScopeType != "undefined" && liveScopeType == "global")
				absoluteImg="";
			for(var i in o.images){
				path=o.images[i].imagePath;
				html+="<div class='col-md-6 padding-5' style='position:relative;height:250px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+"min-height:100%;min-width:100%;height:auto;'></a></div>";
			}
		}
		else if(countImages>=5){
			absoluteImg="position:absolute;";
			if (typeof liveScopeType != "undefined" && liveScopeType == "global")
				absoluteImg="";
			for(var i in o.images){
				path=o.images[i].imagePath;
				if(i==0)
					html+="<div class='col-md-12 no-padding'><div class='col-md-6 padding-5' style='position:relative;height:260px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+"min-height:100%;min-width:100%;'></a></div>";
				else if(i==1){
					html+="<div class='col-md-6 padding-5' style='position:relative;height:260px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+" width:100%;'></a></div></div>";
				}
				else if(i<5){
					html+="<div class='col-md-4 padding-5' style='position:relative;height:160px;overflow:hidden;'><a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'><img src='"+path+"' class='img-responsive' style='"+absoluteImg+"width:100%;'>";
					if(i==4 && countImages > 5){
						diff=countImages-5;
						html+="<div style='position: absolute;top:5px;left:5px;right:5px;bottom:5px;background-color: rgba(0,0,0,0.4);color: white;text-align: center;line-height: 150px;font-size: 50px;'><span>+ "+diff+"</span></div>";
					}
					html+="</a></div>";
				} else{
					html+="<a class='thumb-info' href='"+path+"' data-title='abum de "+targetName+"'  data-lightbox='all"+newsId+"'></a>";	
				}
			}
		}
	}
	return html;
}

function getMediaFiles(o,newsId, edit){
	html="";
	for(var i in o.files){
		path=o.files[i].docPath;
		html+="<div class='col-md-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+
			"<a href='"+path+"' target='_blank'>"+documents.getIcon(o.files[i].contentKey,o.files[i].name)+" "+o.files[i].name+"</a>";
			if(typeof edit != "undefined" && edit=="update"){
				html+="<a href='javascript:;' class='btn-red text-white deleteDocFile deleteDoc'><i class='fa fa-times text-dark'></i></a>"+
					"<input type='hidden' class='docsId' value='"+o.files[i]._id.$id+"'>";
			}
		html +="</div>";
	}
	return html;
}
	
function deleteImage(id,name,hideMsg,communevent){
	var imgToDelete=id;
	if(communevent==true)
		path="communevent";
	else
		path="communecter";
	ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/document/delete/dir/"+moduleId+"/contextType/"+contextParentType+"/contextId/"+contextParentId,
        {"name": name, "parentId": contextParentId, "parentType": contextParentType, "path" : path, "ids" : [id]},
        function(data){ 
        	if(data.result){
				if(hideMsg!=true){
					countImg=$("#resultsImg img").length;
					$("#deleteImg"+imgToDelete).parents().eq(1).remove();
					idImg=countImg-1;
					if(idImg==0){
						$("#resultsImg").empty().hide();
					}
					else{
						$(".count_images").val(idImg);
					}
						toastr.success(data.msg);
				}
			}
			else{
				toastr.error(data.msg);
			}
        }
    );
}

function showNewLinkPreview(newsItem){
    //extract all link from news text
	if(!newsItem.text)
		return null;
		
    // var links =  newsItem.text.match(/(https?:\/\/[^\s]+)/g)
	var linksAll = newsItem.text.match(/(\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig);
	// ES6
	// var uniqueLinks = Array.from(new Set(links)); 

	if (linksAll) {
	var links = [];

	for (var i = 0; i < linksAll.length; i++) {
		if (links.indexOf(linksAll[i]) === -1) {
			links.push(linksAll[i]);
		}
	}

      links.forEach(function(link){
          $.post("/api/news/getpagecontent",{url:link}, function(data){
            var title = data.title ? data.title : "",
                description = data.meta && data.meta.description ? data.meta.description:"",
                ogDescription = data.meta && data.meta["og:description"] ? data.meta["og:description"]:"",
                image = data.meta && data.meta["og:image"] ? data.meta["og:image"] : "",
                url = new URL(link);

			//check url image
			if(image !== ""){
				var r = new RegExp('^(?:[a-z]+:)?//', 'i');
				if(!r.test(image))
					image = (new URL(link)).origin + "/" + image;
			}

            var preview = `
                <div class="link-preview">
					${(image!=="") ? `
						<div class="link-preview-header">
							<img class="link-preview-img" src="${image}" alt="">
						</div>
					`:""}
                    <div class="link-preview-body">
                        <p class="link-preview-domain"><a href="${url.origin}" target="_blank">${url.host}</a></p>
                        <h4 class="link-preview-title"><a href="${link}" target="_blank">${title}</a></h4>
                        <p class="link-preview-description">${
                          description?description:(ogDescription?ogDescription:"")
                        }</p>
                    </div>
                </div>
            `;
            $(`#news${newsItem._id.$id} .timeline-body`).append(preview)
        })
      })
    }
}

$(document).on('keydown', function(e) {
    if (e.ctrlKey && e.key === 'v') {
        $('#get_url').trigger('paste');
    }
});


// function copy paste an image from URL
function setupImageUploader() {
    const imageInput = $("#get_url");

    imageInput.on("paste", async function (e) {
		var event;

		event = e.originalEvent ? e.originalEvent : e;
		if (!event.clipboardData)
			return ;
        const items = event.clipboardData.items;

        for (const item of items) {
            if (item.type.indexOf("image") !== -1) {
                const blob = item.getAsFile();
				showMyImageFromBlob(blob);
				
                // Add a listener to the form submit event
                    e.preventDefault();

                    // Perform the AJAX POST request to save the image
                    try {
                        const response = await saveImage(blob);
                        if (response.result) {
							$(".imagesNews").last().val(response.id.$id);
							$(".imagesNews").last().attr("name","");
							$(".newImageAlbum").last().find("img").removeClass("grayscale");
							$(".newImageAlbum").last().find("i.noGoSaveNews").remove();
							$(".newImageAlbum").last().append("<a href='javascript:;' onclick='deleteImage(\""+response.id.$id+"\",\""+response.name+"\")'><i class='fa fa-times fa-x padding-5 text-white removeImage' id='deleteImg"+response.id.$id+"'></i></a>");
                            toastr.success(response.msg);
                        } else {
                            toastr.error(response.msg);
                        }
                    } catch (error) {
                        toastr.error("Error uploading image: " + error.statusText);
                    }
            }
        }
    });
	

    async function saveImage(blob) {

		if(contextParentType=="city" || contextParentType=="pixels"){
			contextParentType = "citoyens";
			contextParentId = idSession;
		}
		extraParamsAjax={
			contentType: false,
			cache: false, 
			processData: false
		};

        const formData = new FormData();
        formData.append("newsImage", blob, "pasted_image.jpeg");

        return $.ajax({
            type: "POST",
            url: baseUrl + "/" + moduleId + "/document/uploadSave/dir/communecter/folder/" + contextParentType + "/ownerId/" + contextParentId + "/input/newsImage/docType/image/contentKey/slider",
            data: formData,
            contentType: false,
            processData: false
        });
    }
}


function showMyImageFromBlob(blob) {
    if ($(".noGoSaveNews").length) {
        toastr.info(trad.waitendofloading);
    } else if (blob.size > 15000000) {
        toastr.info("Please reduce your image before to 15Mo");
    } else {
        countImg = $("#resultsImg img").length;
        idImg = countImg + 1;
        htmlImg = "";
        
        if (countImg === 0) {
            htmlImg = "<input type='hidden' class='type' value='gallery_images'/>";
            htmlImg += "<input type='hidden' class='count_images' value='" + idImg + "'/>";
            htmlImg += "<input type='hidden' class='algoNbImg' value='" + idImg + "'/>";
            nbId = idImg;
            $("#resultsImg").show();
        } else {
            nbId = $(".algoNbImg").val();
            nbId++;
            $(".count_images").val(idImg);
            $(".algoNbImg").val(nbId);
        }
        
        htmlImg += "<div class='newImageAlbum'><i class='fa fa-spin fa-circle-o-notch fa-3x text-green spinner-add-image noGoSaveNews'></i><img src='' id='thumbail" + nbId + "' class='grayscale' style='width:75px; height:75px;'/>" +
            "<input type='hidden' class='imagesNews docsId' name='goSaveNews' value=''/></div>";
        
        $("#resultsImg").append(htmlImg);
        var img = document.getElementById("thumbail" + nbId);
        img.file = new File([blob], "image.jpg", { type: blob.type });
        img.src = URL.createObjectURL(blob);
    }
}
