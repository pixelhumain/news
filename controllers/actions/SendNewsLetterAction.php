<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\NewsLetter;
use Rest;

class SendNewsLetterAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($type, $id) {
        try{
            $requiredParameters = ["receivers", "content", "subject"];
            foreach($requiredParameters as $param){
                if(!isset($_POST["mail"][$param]))
                    throw new Exception("'".$param."' parameter missing.");
            }
            
            NewsLetter::send(
                $_SESSION["userId"], 
                $type,
                $id,
                $_POST["mail"]
            );

            return Rest::json(["error"=>false]);
        }catch(Exception $e){
            return Rest::json(["error"=>true, "message"=>$e->getMessage()]);
        }
    }

}