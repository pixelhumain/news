<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, Element, Rest, Yii, MongoId, Citoyen, Log, Person, Authorisation;
use IpSpam;
use PHDB;

class NoSpamAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {

        if ( !Person::logguedAndValid() ||  (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) 
            return Rest::json( array( "result" => false, "msg" => Yii::t("common", "Access denied") ));

    $res=array("result"=>false, "msg"=> "params incomplete");

    if(isset($_POST["id"]) && isset($_POST["type"])){
        $res = IpSpam::noSpamElement($_POST["id"], $_POST["type"]);
    }
    
    return Rest::json($res);
    }
}