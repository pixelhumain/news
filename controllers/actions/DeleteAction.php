<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;
use CAction;
use News;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\Activitypub;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Rest;
use Yii;

class DeleteAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {
    	//Check if connected
        $controller=$this->getController();
        if( ! Person::logguedAndValid()) {
            $res = array("result"=>false, "msg"=>"You must be loggued to delete a comment");
            return Rest::json( $res );
        } else {
        	$res = News::delete($id, Yii::app()->session["userId"], true);
            if($activity = ActivitypubActivityCreator::createDeleteNoteActivity($id))
                Handler::handle($activity);

        	if(@$res["newsUp"] && $_POST["isLive"]){
        		$params=array(
    			"news"=>array( $id=>$res["newsUp"]), 
    			"actionController"=>"save",
    			"canManageNews"=>true,
    			"canPostNews"=>true,
                "nbCol" => 1,
                "pair" => false);
				return $controller->renderPartial("newsPartialCO2", $params,true);
        	}else
                return Rest::json( $res );
        }
    }
}