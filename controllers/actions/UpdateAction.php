<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;
use CAction;
use News;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\Activitypub;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Rest;

class UpdateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$controller=$this->getController();
    	$result=News::update($_POST);

		//federation de l'activité
		if($activity = ActivitypubActivityCreator::createUpdateNoteActivity($_POST)){
			Handler::handle($activity);
		}

    	if(@$_GET["tpl"]=="co2"){
    		$params=array(
    			"news"=>array((string)$result["object"]["_id"]=>$result["object"]), 
    			"actionController"=>"save",
    			"canManageNews"=>true,
    			"canPostNews"=>true,
                "nbCol" => 1,
                "endStream"=>false,
                "pair" => false);
			return $controller->renderPartial("news.views.co.timelineTree", $params,true);
    	}
		else
        	return Rest::json($result);
    }
}