<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;

class NewsTranslator {

	const NEWS_CONTRIBUTORS = "newsContributors";
	const NEWS_CREATE_PROJECT = "newsCreateProject";
	const NEWS_CREATE_NEED = "newsCreateNeed";
	const NEWS_CREATE_EVENT = "newsCreateEvent";
	const NEWS_CREATE_ORGANIZATION = "newsCreateOrganization";
	const NEWS_CREATE_TASK = "newsCreateTask";
	const NEWS_JOIN_ORGANIZATION = "newsMemberJoinOrganization";
	/**
		*get news well formated
		*	@param string author become an object,
		*	@param object object (if type=="activityStream") become object with informations,
		*	@param object target,
		*	@param object media (if type=="gallery_images") become of object with document,
	**/
	public static function convertParamsForNews($params,$readOne=false, $followsArrayIds=null){

		if(isset($params["object"]["type"])){

			$docImg="";
			if($params["object"]["type"]==News::COLLECTION){
				$object=News::getById((string)$params["object"]["id"]);
			}else{
				$fields = array("name", "collection", "slug", "updated", "profilThumbImageUrl", "profilMediumImageUrl", "shortDescription", "tags", "type", "section", "category", "startDate", "endDate", "openingHours", "address", "geo", "geoPosition","links");
				$object=Element::getElementSimpleById($params["object"]["id"], $params["object"]["type"], null, $fields);
			}
			$params["icon"]="fa-".Element::getFaIcon($params["object"]["type"]);
			if(!empty($object)){
				$thisType = $params["object"]["type"];
				$params["object"] = array_merge($params["object"], $object);
				$params["object"]["type"] = $thisType;
				$params["imageBackground"] = @$object["profilImageUrl"];
				$params["name"] = @$object["name"] ? $object["name"] : @$object["title"];
				if($params["object"]["type"]==Event::COLLECTION){
					$params["startDate"]=@$object["startDate"];
					$params["endDate"]=@$object["endDate"];
					$params["startDateSec"]=@$object["startDateSec"];
					$params["endDateSec"]=@$object["endDateSec"];

				}

				if($params["object"]["type"]==News::COLLECTION){
					//$params["text"] = $params["object"][""]
					//var_dump($params["object"]); exit;
					$params["object"] = array_merge($params["object"], $object);

				}

				if(isset($object["address"]))
					$params["scope"]["address"]=$object["address"];


			}else{
				$params=array();
				return $params;
			}
		}
		if(@$params["target"]["type"]){
			$fields=array("name","profilThumbImageUrl");
			$target=Element::getElementSimpleById($params["target"]["id"], $params["target"]["type"],null, $fields);
			$params["target"] = array("id"=>@$params["target"]["id"],
									 "name"=>@$target["name"],
									 "type"=>@$params["target"]["type"],
									 "profilThumbImageUrl"=>@$target["profilThumbImageUrl"]);

		}
		if((isset($params["media"]) && $params["media"]["type"]=="gallery_images") || isset($params["mediaImg"])){
			if(isset($params["mediaImg"]))
				$mediaImg= $params["mediaImg"];
			else{
				$mediaImg=$params["media"];
				unset($params["media"]);
			}
			$images=array();
			$limit=5;
			$i=0;
			if(!empty($mediaImg["images"])){
				foreach($mediaImg["images"] as $data){
					if($i<$limit){
						if(@$data && !empty($data)){
							$image=Document::getById($data);
							if(@$image){
								$image["imagePath"]=Document::getDocumentPath($image, true);
								$image["imageThumbPath"]=Document::getDocumentPath($image, true, Document::GENERATED_IMAGES_FOLDER."/");
								array_push($images,$image);
							}else{
								$countImages=intval($mediaImg["countImages"]);
								$countImages--;
								$mediaImg["countImages"]=$countImages;
							}
						}else{
							$countImages=intval($mediaImg["countImages"]);
							$countImages--;
							$mediaImg["countImages"]=$countImages;
						}
					} else {
						exit;
					}
				}
			}
			$params["mediaImg"]=$mediaImg;
			$params["mediaImg"]["images"]=$images;
		}
		if((isset($params["media"]) && $params["media"]["type"]=="gallery_files") || isset($params["mediaFile"])){
			if(isset($params["mediaFile"]))
				$mediaFile= $params["mediaFile"];
			else{
				$mediaFile=$params["media"];
				unset($params["media"]);
			}
			$files=array();
			$limit=5;
			$i=0;
			if(isset($mediaFile["files"]) && !empty($mediaFile["files"])){
				foreach($mediaFile["files"] as $data){
					if($i<$limit){
						if(@$data && !empty($data)){
							$file=Document::getById($data);
							if(@$file){
								$file["docPath"]=Document::getDocumentPath($file, true);
								array_push($files,$file);
							}else{
								$countFiles=intval($mediaFile["countFiles"]);
								$countFiles--;
								$mediaFile["countFiles"]=$countFiles;
							}
						}else{
							$countFiles=intval($mediaFile["countFiles"]);
							$countFiles--;
							$mediaFile["countFiles"]=$countFiles;
						}
					} else {
						exit;
					}
				}
			}
			$params["mediaFile"]=$mediaFile;
			$params["mediaFile"]["files"]=$files;
		}
		else if(@$params["media"] && $params["media"]["type"]=="activityStream"){
			$element=Element::getSimpleByTypeAndId($params["media"]["object"]["type"], $params["media"]["object"]["id"]);
			$element["type"]=$params["media"]["object"]["type"];
			if($params["media"]["object"]["type"]==Event::COLLECTION)
				$element["typeEvent"] = @$element["type"];
			$params["media"]["object"] = $element;
		}

		if(!isset($params["author"]["id"]) || @$params["verb"] == "create"){
			$authorId=@$params["author"];
			$authorType=Person::COLLECTION;
			$fields=array("name","profilThumbImageUrl", "geo");
			if(@$params["targetIsAuthor"]==true || @$params["verb"] == "create"){
				$authorId=$params["target"]["id"];
				$authorType=$params["target"]["type"];
				$author=Element::getElementSimpleById( $params["target"]["id"],$params["target"]["type"],null, $fields);
	  			$params["authorName"] = @$author["name"];
	  			$params["authorId"] = @$params["target"]["id"];
	  			$params["authorType"] = @$params["target"]["type"];
	  			$params["updated"] = $params["created"];
	  			$params["sharedBy"] = array();
			}else{
                if (filter_var($authorId, FILTER_VALIDATE_URL)) {
                    $author = ActivitypubTranslator::actorTocoUser($authorId);
                    $authorType = "activitypub";
                } else {
				    $author=Element::getElementSimpleById( $params["author"],Person::COLLECTION,null, $fields);
                }
  			}
	  	}else{
	  		$author = $params["author"];
	  	}

	  	$author = array("id"=>@$authorId,
					    "geo"=>@$author["geo"],
					    "name"=>@$author["name"],
					    "type"=>@$authorType,
					    "profilThumbImageUrl"=>@$author["profilThumbImageUrl"]);

	  	if(@$author["geo"]==null) unset($author["geo"]);

	  	//var_dump($params["author"]); //exit;
  		if (!empty($author)) $params["author"] = $author;
	  	else return array("created"=>$params["created"]);

		if(isset($params["sharedBy"])){
			$sharedBy = array();
			$dateUpdated = @$params["updated"];
			$lastComment = "";
			$lastAuthorShare = array();
			$lastKey = null;

			$count=0;
			foreach($params["sharedBy"] as $key => $value){
				 //on commence par prendre la date du premier partage (date de création si news)
				if($count==0) $dateUpdated = @$value["updated"];
				$count++;

				$lastKey = null;
				$fields=array("name","profilThumbImageUrl");
				$share=Element::getElementSimpleById($value["id"],$value["type"],null, $fields);

				//$share =  Element::getSimpleByTypeAndId($value["type"], $value["id"]);
				if (!empty($share)){
					$clearShare = array("id"=>@$value["id"],
										"name"=>@$share["name"],
										"type"=>@$value["type"],
										"updated" => @$value["updated"],
										"comment" => @$value["comment"],
										"profilThumbImageUrl"=>@$share["profilThumbImageUrl"]);
					$lastAuthorShare = $clearShare;	  //memorise l'auteur du share
					if(@$followsArrayIds){ //si j'ai la liste des follows de l'element
						//et que l'id du sharedBy est dans la liste des follows
						//ou que l'id du sharedBy est mon id
						if( in_array(@$value["id"], $followsArrayIds) ||
									 @$value["id"] == Yii::app()->session["userId"]){
							$dateUpdated = $value["updated"]; //memorise la date du share
							$lastComment = @$value["comment"]; //memorise la date du share
							$lastAuthorShare = $clearShare;	  //memorise l'auteur du share
							$lastKey = count($sharedBy);
						}
					}else if(@$value["id"] == Yii::app()->session["userId"]){//si j'ai pas la liste des follows de l'element => journal
						$dateUpdated = $value["updated"]; //memorise la date du share
						$lastComment = @$value["comment"]; //memorise la date du share
						$lastKey = count($sharedBy);
					}else{
						$lastComment = @$value["comment"];
					}

					//ajoute le share dans la liste
					$sharedBy[] = $clearShare;
					//memorise la clé pour pouvoir supprimer le dernier share de la liste
					//(permet d'afficher le bon nombre de partage)
				}
			}

			//efface le lastAuthorShared de la liste des sharedBy
			//echo $lastKey;
			//if($lastKey!=null && @$sharedBy[$lastKey]){ unset($sharedBy[$lastKey]); }

			$params["updated"] = @$dateUpdated;
			$params["comment"] = @$lastComment;
			$params["typeSig"] = "news";

		  	if(!empty($lastAuthorShare)){
		  		$params["lastAuthorShare"] = @$lastAuthorShare;
		  	}
		  	else if(!isset($followsArrayIds)){
		  		$params["lastAuthorShare"] = @$author;
		  	}


		  	$params["sharedBy"] = $sharedBy;

		}
		return $params;
	}
}
