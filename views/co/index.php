<style type="text/css">
  <?php if($nbCol == 1){ ?>
    .timeline > li{
      width:100%;
    }
    .timeline::before {
      left:0;
    }
  <?php } ?>

.tag-input-container{
      overflow-x: auto;
      -webkit-writing-mode: horizontal-tb !important;
      text-rendering: auto;
      letter-spacing: normal;
      word-spacing: normal;
      text-transform: none;
      text-indent: 0px;
      text-shadow: none;
      display: flex;
      text-align: start;
      -webkit-appearance: textfield;
      -webkit-rtl-ordering: logical;
      cursor: text;
      margin: 0em;
      padding: 1px 2px;
      border-width: 1px;
      border-image: initial;
      white-space: nowrap;
      align-items: center;
      justify-content: flex-start;
      max-width: 100%;
      scrollbar-width: none;
  }
  .tag-input-container::-webkit-scrollbar {
      display: none;
  }
  .tag-input-container .data-tag-container{
      display: inline-block;
  }
  .tag-input-container .data-tag{
      display: inline-block;
      background-color: #fff;
      margin-right: 10px;
      cursor: pointer;
      font-weight: 700;
      font-size: 18px;
      float: left;
      width: auto;
      padding: 4px 14px;
      color: #9fbd38;
      text-decoration: none;
      text-align: center;
      border: 2px solid #9fbd38;
      margin-bottom: 0px;
      border-radius: 40px;
  }
  .tag-input-container .data-tag:hover{
      background: #9fbd38;
      color:#fff;
  }
  .tag-input-container .data-tag.active{
      background: #9fbd38;
      border: none;
      color:#fff;
  }
</style>
<?php 
	$class="";
	if(@$inline && $inline) $class.="inline";
	if(@$nbCol && $nbCol) $class.=" nb-col-".$nbCol;
?> 
<div class="col-xs-12 no-padding container-live <?php echo $class ?>">

    <?php
        if($formCreate!=="false"){ ?> 
		<div class="col-xs-12 no-padding margin-bottom-15">
		<?php 
	        $params = array(
	                  "contextParentId" => @$contextParentId,
	                  "parent" => @$parent,
	                  "contextParentType" => @$contextParentType,
	                  "canAddNews" => @$canAddNews,
	                  "canManageNews" => @$canManageNews,
	                  "isLive" => @$isLive,
	                  "nbCol"=>@$nbCol,
	                  "authorizedToStock" => @$authorizedToStock
	          );
	        echo $this->renderPartial('formCreateNews', $params);
	  	?>
		</div>
	<?php } ?>

    <?php
    if(isset($tagFediverses) && count($tagFediverses) > 0) { ?>
        <div class="tag-input-container">
            <?php
                foreach($tagFediverses as  $item){ ?>
                    <div data-item="<?php echo $item; ?>" class="data-tag">
                        <?php echo $item;?>
                    </div>
            <?php } ?>
    </div>
    <?php } ?>
	<ul class="timeline inline-block" id="news-list">

	</ul>
</div>

<?php echo $this->renderPartial('modalModeration', array());
	if( isset(Yii::app()->session["userId"]) ) echo $this->renderPartial('modalShare', array()); ?>


<!-- this script is for image exif for checking the rotation -->
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>

<script type="text/javascript" >
	modules.news = <?php echo json_encode( News::getConfig() ) ?> ;
	var news = {};
	var newsScopes={};
	var loadingData = false;
	var isLive = <?php echo json_encode(@$isLive) ?>;
	var source = <?php echo json_encode(@$source) ?>;
	var indexStepNews = <?php echo json_encode(@$indexStep); ?>;
	var typeNews = <?php echo json_encode(@$typeNews); ?>;
	var inline = <?php echo json_encode(@$inline); ?>;
	var scrollDom = <?php echo json_encode(@$scrollDom); ?>;
	var members = <?php echo json_encode(@$members) ?>;
	var scroll = <?php echo json_encode(@$scroll) ?>;
	var searchTags = <?php echo json_encode(@$searchTags) ?>;
	var searchTypes = <?php echo json_encode(@$searchTypes) ?>;
	var nbCol = parseInt(<?php echo json_encode(@$nbCol); ?>);
	var contextParentType = <?php echo json_encode(@$contextParentType) ?>;
	var contextParentId = <?php echo json_encode(@$contextParentId) ?>;
	var searchInNews = <?php echo json_encode(@$search) ?>;
	var canPostNews = <?php echo json_encode(@$canPostNews) ?>;
	var timelineHtml = <?php echo json_encode(@$timelineHtml) ?>;
	var canManageNews = <?php echo json_encode(@$canManageNews) ?>;
	var uploadUrl = "<?php echo Yii::app()->params['uploadUrl'] ?>";
    // tag fediverse
    var fediTags = <?php echo json_encode(@$fediTags) ?>;
	var docType="<?php echo Document::DOC_TYPE_IMAGE; ?>";
	var contentKey = "<?php echo Document::IMG_SLIDER; ?>";
	var urlData=baseUrl+"/news/co/get";
	if(notNull(isLive))
		urlData+="/isLive/"+isLive;
	if(notNull(contextParentType) && notNull(contextParentId))
		urlData+="/type/"+contextParentType+"/id/"+contextParentId;
	if(notNull(source))
		urlData+="/source/"+source
	var paramsSearch = {
	 	container : "#filters-nav",
	 	urlData : urlData,
	 	loadEvent: {
	 		default : "live",
	 		eventType: "scroll",
			eventTarget: null,	
	 	},
	 	live : {
	 		options:{
	 			type:contextParentType,
				id: contextParentId,
				nbCol : nbCol,
				search : searchInNews,
				indexStep: indexStepNews,
				inline : inline,
				timelineHtml:timelineHtml,
				isLive : isLive,
				members : members,
				scroll : scroll,
				source : source,
				typeNews : typeNews,
				searchTags : searchTags,
                fediTags: fediTags
	 		}
 		}, 
 		results : {
 			dom : "#news-list"
 		},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		type : {
	 			name : "Publication",
	 			view : "dropdownList",
	 			event : "selectList",
	 			type : "type",
	 			list : {
	 				"news": "news",
	 				"activityStream" : "activityStream"
	 			}
	 		},
	 		themes : {
	 			lists : <?php echo json_encode(CO2::getContextList("filliaireCategories")) ?>
	 		}
			 		
	 	}
	};
	if(searchTypes===false || searchTypes==="false"){
		delete paramsSearch.filters.type
	}
	if(typeof liveParams != "undefined" && typeof liveParams.extendFilters != "undefined" )
		$.extend(paramsSearch.filters, liveParams.extendFilters);
	// if(typeof liveParams != "undefined" && typeof liveParams.filters != "undefined" ){
    //     paramsSearch.filters = {};
    //     $.each(liveParams.filters,function(kf,vf){
    //         if(kf == "themes" && vf == true){
    //             paramsSearch.filters[kf] = {};
    //             paramsSearch.filters[kf].lists = <?php //echo json_encode(CO2::getContextList("filliaireCategories")) ?>;
    //         }else{
    //             paramsSearch.filters[kf] = vf;
    //         }
    //     })
    // }
	var filterSearch={};
    jQuery(document).ready(function() {
		mylog.log("Live engine");
		newsObj.initForm();
		newsObj.bindEvent();

		filterSearch = searchObj.init(paramsSearch);
		filterSearch.search.init(filterSearch);
		if(wsCO){
			wsCO.on('new-post', function(){
				filterSearch.search.init(filterSearch);
			})
		}
        var tagFilters = "<?php echo isset($tagFediverses)  ? count($tagFediverses) : 0 ?>";
        if(parseInt(tagFilters) > 0){
        var container = document.querySelector('.tag-input-container');
        var selectedTags = [];
        container.addEventListener('click', function(event) {
            if (event.target.classList.contains('data-tag')) {
                var clickedTag = event.target;
                var tagIndex = selectedTags.indexOf(clickedTag.dataset.item);

                if (tagIndex === -1) {
                    selectedTags.push(clickedTag.dataset.item);
                    clickedTag.classList.add('active');
                } else {
                    selectedTags.splice(tagIndex, 1);
                    clickedTag.classList.remove('active');
                }

                container.scrollTo({
                    left: event.target.offsetLeft - container.offsetWidth/2 + event.target.offsetWidth/2,
                    behavior: 'smooth'
                });
                paramsSearch.live.options.fediTags = selectedTags;
                filterSearch = searchObj.init(paramsSearch);
                filterSearch.search.init(filterSearch);
            }
        });
        }
    });

	/*jQuery(document).ready(function() {

		newsObj.config={
			searchTags : searchTags
		};
	    newsObj.init();  
	});*/
</script>