<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, Element, Rest, Yii, MongoId, Authorisation,Person, DataValidator, CTKException, Log, MongoDate;
use Citoyen;
use PHDB;
use IpSpam;

class AddIpSpamAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {
        if ( ! Person::logguedAndValid() ||  (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) 
            return Rest::json( array( "result" => false, "msg" => Yii::t("common", "Access denied") ));

        $ipspam = $_POST["ipspam"];

        $res=array("result"=>false, "msg"=> "params empty");
		
        if(isset($_POST["type"]) && isset($_POST["id"])){
            $res = IpSpam::addElementToSpam($_POST["id"], $_POST["type"]);
        }
        
        foreach($ipspam as $ip){
            $res = IpSpam::insertByIp($ip);
        }

		return Rest::json($res);
    }   
}