<?php

namespace PixelHumain\PixelHumain\modules\news\controllers\actions;

use CAction, Element, Rest, Yii, MongoId, Citoyen, Log, Person, Authorisation;
use PHDB;

class DeleteSpamAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($id= null) {

        if ( !Person::logguedAndValid() ||  (!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) 
            return Rest::json( array( "result" => false, "msg" => Yii::t("common", "Access denied") ));

        $res=array("result"=>false, "msg"=> "params incomplete");

        if(isset($_POST["id"]) && isset($_POST["type"])){
            $id = $_POST["id"];
            $type = $_POST["type"];

            $element = Element::getByTypeAndId( $type, new MongoId($id));
            //if((isset($element["slug"]) && $element["slug"] != "unknown") || (isset($element["collection"]) && $element["collection"] == "poi")){
                $res= Element::deleteSpam($type, $id, @$element["reasonDelete"], @$element["userAskingToDelete"], $element);	
            //}

        }
        
		return Rest::json($res);
    }
}